#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakSDL.h"
#include "FuntzioakLobby.h"

#include "FuntzioakRedes.h"
#include "FuntzioakOinarri.h"
#include "FuntzioakMate.h"
#include "FuntzioakFisika.h"
#include "FuntzioakProgram.h"

#include "FuntzioakRedesBoss.h"
#include "FuntzioakOinarriBoss.h"
#include "FuntzioakFisikaBoss.h"
#include "FuntzioakMateBoss.h"
#include "FuntzioakProgramBoss.h"

int main(int argc, char* argv[])
{
	irudia = NULL;
	Superficie = NULL;
	screen = NULL;

	int idAudio;

	kendu = 0;

	if (!Hasieratu()) {
		printf("Ezin izan da hasieratu\n");
	}
	else {		
		kutxaIrekita = 0;
		rekuEginda = 0;
		apunteKop = 0;

		audioInit();
		idAudio = loadTheMusic(".\\Soca.wav");
		playMusic();
	
		KargaPantaila();
	}

	itxi();

	return 0;
}
