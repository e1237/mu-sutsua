#ifndef FUNTZIOAKFISIKA_H
#define FUNTZIOAKFISIKA_H

void mainFisika();

void pertsonaiaMarraztuF(char helbidea[]);
void mapaMarraztuF();

void PertsonaiaEzkerreraF();
void PertsonaiaEskumaraF();

void PertsonaiaEzkerreraSaltoF(int Y);
void PertsonaiaEskumaraSaltoF(int Y);

void kolisioaDetektatuF(int x, int y);

void SaltoaMarraztuF(float t0, int y0, int vy);
void jausiF();
#endif