#ifndef FUNTZIOAKREDESBOSS_H
#define FUNTZIOAKREDESBOSS_H

void mainRedesBoss();

void pertsonaiaMarraztuRB(char helbidea[]);
void bossaMarraztuRB();
void XabiMarraztuRB();
void XabiMugituRB();
void TxanponaLortuRB();
void TxanponaMugituRB();


void erasoaMarraztuRB(char helbidea[]);
void erasoaMugituRB();

void mapaMarraztuRB();

void EzkerreraRB();
void EskumaraRB();

void PertsonaiaEzkerreraSaltoRB(int Y);
void PertsonaiaEskumaraSaltoRB(int Y);

void kolisioaDetektatuRB(int x, int y);

void SaltoaMarraztuRB(float t0, int y0, int vy);
void jausiRB();
void hilRB(int nor);

void hilF(int nor);

#endif