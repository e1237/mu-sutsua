#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakLobby.h"
#include "FuntzioakFisikaBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakFisika.h"

int norabidea;

void mainFisika()
{
	//aldagai globalen deklarazioa
	pertsonaiaX = 120;
	pertsonaiaY = 449 - 125;
	mapaX = 0;

	pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten)) //bukle nagusia. bukle hau jokoa martxan dagoen bitartean ez da geldituko. Mugimendu sistema
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuF(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					PertsonaiaEzkerreraF();
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraF();
					break;
				}
			}
		}
	}

	if (irten == 3)
	{
		SDL_Delay(500);
		irten = 0;
		mainFisikaBoss();
	}
	else if (irten == 2)
	{
		irten = 0;
		if (bizitzaF > 0)
		{
			bizitzaF--;
			SDL_Delay(500);
			mainFisika();
		}
		else if (bizitzaF == 0)
		{
			SDL_Delay(500);
			mainLobby();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaF = 0;
		mainLobby();
	}
	else
	{
		bizitzaF = 0;
	}
}

void pertsonaiaMarraztuF(char helbidea[])
{
	mapaMarraztuF(); //mapa marraztu

	kargatuPertsonaia(helbidea); //pertsonaia marraztu

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaF);  //pertsonaiaren bizitza marraztu
	//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);

	//EtsaienMugimendua();
}

void mapaMarraztuF()
{
	//mapa bi marrazkitan banatuta dago
	if (mapaX + pertsonaiaX <= 5000)
	{
		kargatuIrudia(".\\Argazkiak\\Fisika-bmp\\FisikaNibela1.bmp"); //maparen lehen zatia marraztu

		SDL_Rect mapa;

		mapa.x = mapaX;
		mapa.y = 0;
		mapa.w = 1200;
		mapa.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	}
	else
	{
		kargatuIrudia(".\\Argazkiak\\Fisika-bmp\\FisikaNibela2.bmp"); //maparen bigarren zatia marraztu

		SDL_Rect mapa2;

		mapa2.x = mapaX - 4000;
		mapa2.y = 0;
		mapa2.w = 1200;
		mapa2.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa2, NULL);
	}

	/*SDL_Rect dstRect;

	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = 1200;
	dstRect.h = 600;*/

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);
}

void PertsonaiaEzkerreraF()
{
	kolisioaDetektatuF(pertsonaiaX - 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");//pertsonaia maparen erdida heltzen denean, mapa mugituko da
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 2;
		jausiF();
	}
}
void PertsonaiaEskumaraF()
{
	kolisioaDetektatuF(pertsonaiaX + 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik


	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))//pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			pertsonaiaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 1;
	}
	jausiF();
}

void PertsonaiaEzkerreraSaltoF(int Y)
{
	kolisioaDetektatuF(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			mapaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaX -= 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		norabidea = 2;
	}
}
void PertsonaiaEskumaraSaltoF(int Y)
{
	kolisioaDetektatuF(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 10;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			mapaX += 10;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaX += 15;
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		norabidea = 1;
	}
}

void kolisioaDetektatuF(int x, int y)
{
	//mapan dauden kolisio guztiak kalkulatzen ditu. Horrela mapari sentzua emanez.
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 10000))
	{
		kolisioa = 1;
	}
	else if ((y - 125 > 600))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x < 900))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 900) && (x < 1200))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x > 1200) && (x - 30 < 1799))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 30 > 1950) && (x - 30 < 2099))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 30 > 2250) && (x - 30 < 2399))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 30 > 2588) && (x - 30 < 2707))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 30 > 2850) && (x - 30 < 2999))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 30 > 3150) && (x - 30 < 3299))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x + 30 > 3299) && (x < 3599))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 3599) && (x < 5400))
	{
		kolisioa = 1;
	}
	else if ((y > 245) && (y - 115 < 300) && (x > 5120) && (x < 5400))
	{
		kolisioa = 1;
	}
	else if ((y > 150) && (x > 5400) && (x < 5699))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x > 5850) && (x < 5999))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 6150) && (x < 6900))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 6900) && (x < 7050))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x > 7050) && (x < 8549))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 8549) && (x < 8699))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 8699) && (x < 10000))
	{
		kolisioa = 1;
	}
	else if ((y > 277) && (x > 9681) && (x < 9771))
	{
		irten = 3;
	}
 }

void SaltoaMarraztuF(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT://bi funtzio hauen eta goiko bi funtzio haien diferentzia nagusia, hanken mugimendua da. Funtzio hauetan, hankak ez dira mugitzen ezkerrera edo eskumara egitean.
					PertsonaiaEzkerreraSaltoF(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoF(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuF(pertsonaiaX, Y);

	if (kolisioa == 0)
	{
		pertsonaiaY = Y;
		if (norabidea == 2)
		{
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuF(t0, y0, vy);
	}
	else
	{
		jausiF();
	}
}
void jausiF()
{
	kolisioaDetektatuF(pertsonaiaX, pertsonaiaY + 7);

	while (kolisioa == 0)//kolisioa ez dagoen bitartean, lurra ez dagoen bitartean, pertsonaia jausi egingo da.
	{
		pertsonaiaY += 7;
		if (norabidea == 1)
		{
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuF(pertsonaiaX, pertsonaiaY + 7);
	}

	if (pertsonaiaY >= 550)// hemen sartzen bada, pertsonaia hutsunera jausi egin da.
	{
		if (bizitzaF > 0)
		{
			irten = 2;
		}
		else
		{
			irten = 2;
		}
	}

	pertsonaiaY += 7;
	kolisioaDetektatuF(pertsonaiaX, pertsonaiaY);
	while (kolisioa == 1)
	{
		kolisioaDetektatuF(pertsonaiaX, pertsonaiaY - 1);
		pertsonaiaY -= 1;
	}

	if (norabidea == 1)
	{
		pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuF(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
}