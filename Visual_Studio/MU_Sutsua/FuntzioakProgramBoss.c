#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakProgramBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakLobby.h"

int BossPosturaP = 0;
int BaAlDago579, Non579, Zein579;
int helduDa;

void mainProgramBoss()
{
	//aldagaien deklarazioa
	int Media;

	pertsonaiaX = 120;
	pertsonaiaY = 447 - 125;
	mapaX = 0;

	bizitzaBoss = 3;

	erasoaX = 20;
	erasoaY = -160;

	bossX = 1200;
	bossY = 447 - 179;

	kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	pertsonaiaMarraztuPB();

	SDL_Event e;

	while ((!kendu) && (!irten))
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{//pertsonaiaren mugimendu sistema. Informazioa teklatutik sartu
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuPB(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					EzkerreraPB();
					break;
				case SDLK_RIGHT:
					EskumaraPB();
					break;
				}

			}
		}

		if (helduDa == 1) //pertsonaia puntu batera heltzean, etsai nagusiak erasoak hasi
		{
			if (erasoaY < 600) //0roak lurrera heldu arte, beherentz joango dira
			{
				erasoaMugituPB();
			}
			else
			{
				BaAlDago579 = randomizatuPB(2); //normalean 0roak jausiko dira, baina batzuetan, 
				//hausazko posizioetan, beste zenbaki batzuk
				if (BaAlDago579 == 1)
				{
					Non579 = randomizatuPB(7);
					Zein579 = randomizatuPB(3);
				}
				else
				{
					Non579 = 7;
					Zein579 = 4;
				}
				erasoaY = -160;
			}
		}
	}

	if (irten == 2)
	{
		hilPB(1);
		irten = 0;
		ureztatu = 1;
		mainLobby();
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaP = 0;
		mainLobby();
	}
	else if (irten == 3)
	{
		irten = 0;
		SDL_Delay(500);
		ureztatu = 1;
		Media = Mediakalkulatu();
		printf("\nRedes: %d\nOinarri: %d\nMate: %d\nFisika: %d\nProgram: %d\nBataz Bestekoa: %d\n", bizitzaR, bizitzaO, bizitzaM, bizitzaF, bizitzaP, Media);
		if (Media >= 5)
		{
			KargaPantailaMarraztu(".\\Argazkiak\\Pantailak-bmp\\Gainditu.bmp");
			SDL_Delay(3000);
			bukaerakoNotaMarraztu(Media);
			partidaIdatzi();
			KargaPantaila();
		}
		else
		{
			KargaPantailaMarraztu(".\\Argazkiak\\Pantailak-bmp\\Errepikatu.bmp");
			SDL_Delay(10000);
			mainLobby();
		}
	}
	else
	{
		bizitzaP = 0;
	}
}

void mapaMarraztuPB()
{
	kargatuIrudia(".\\Argazkiak\\Program-bmp\\Tumba.bmp");

	SDL_Rect mapa;

	mapa.x = mapaX;
	mapa.y = 0;
	mapa.w = 1200;
	mapa.h = 600;

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
}

void pertsonaiaMarraztuPB()
{
		mapaMarraztuPB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		bossaMarraztuPB();

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaP);
		bihotzaMarraztu(bizitzaBoss);

		SDL_RenderPresent(Render);
}

void EzkerreraPB()
{
	KolisioaDetektatuPB(pertsonaiaX - 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900)) //pertsonaia pantailaren erdira heltzen denean, mapa mugitzen hasi
		{
			mapaX -= 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			mapaX -= 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
		else //hasiera batean, pertsonaia mugituko da. Pantailaren erdira heldu arte
		{
			pertsonaiaX -= 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			pertsonaiaX -= 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
	}
}

void EskumaraPB()
{
	KolisioaDetektatuPB(pertsonaiaX + 30, pertsonaiaY);
	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))//pertsonaia pantailaren erdira heltzean, mapa mugitzen hasi
		{
			mapaX += 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			mapaX += 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
		else //hasiera batean, pertsonaia mugituko da. Pantailaren erdira heldu arte.
		{
			pertsonaiaX += 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			pertsonaiaX += 15;
			kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
	}

	if ((helduDa == 0) && (pertsonaiaX + mapaX >= 800))
	{
		helduDa = 1;
	}
}

void SaltoaMarraztuPB(int t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	erasoaMugituPB();

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{//saltoko ezker eta eskuin mugimenduak. Besteekiko diferentzia bakarra: hauek ez dituzte hankak mugitzen
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					PertsonaiaEzkerreraSaltoPB(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoPB(Y);
					break;
				}
			}
		}
	}

	KolisioaDetektatuPB(pertsonaiaX, Y);

	if (kolisioa == 0) {
		pertsonaiaY = Y;
		pertsonaiaMarraztuPB();

		SaltoaMarraztuPB(t0, y0, vy);
	}
	else
	{
		jausiPB();
	}
}

void PertsonaiaEzkerreraSaltoPB(int Y)
{
	KolisioaDetektatuPB(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
	}
}

void PertsonaiaEskumaraSaltoPB(int Y)
{
	KolisioaDetektatuPB(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		kargatuPertsonaia(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuPB();
			SDL_Delay(50);
		}
	}
}

void KolisioaDetektatuPB(int x, int y)
{
	y += 125;
	x += mapaX + 35;

	kolisioa = 0;

	if ((x - 35 <= 0) || (x + 35 >= 1320))
	{
		kolisioa = 1;
	}
	else if (y > 447)
	{
		kolisioa = 1;
	}
	else if ((bizitzaBoss == 0) && (x - 35 >= bossX))
	{
		irten = 3;
	}

	erasoarekinKolisioaPB(x, y);
}

void erasoarekinKolisioaPB(int x, int y)
{
	int i = 0;
	int erasoarekinKolisioa = 0;
	
	while ((i <= 5) && (erasoarekinKolisioa == 0)) //5 zenbakia, jausiko dira. Gehiengoak 0roak.
	{
		if ((erasoaX + i * 200 < pertsonaiaX + mapaX + 35) && (erasoaX + i * 200 > pertsonaiaX + mapaX - 35) && (erasoaY > pertsonaiaY) && (erasoaY < pertsonaiaY + 125))
		{
			if (i == Non579) //0 eta pertsonaiaren arteko kolisioa dagoenean, pertsonaiak bizitza galdu
			{
				bizitzaBoss--;
				if (bizitzaP < 10)
				{
					bizitzaP++;
				}
			}
			else//beste zenbaki batean eta pertsonaiaren arteko kolisioa dagoenean, etsai nagusiak bizitza galdu
			{
				if (bizitzaP > 0)
				{
					bizitzaP--;
				}
				else
				{
					irten = 2;
				}
			}
			erasoarekinKolisioa = 1;
		}

		i++;
	}

	if (erasoarekinKolisioa == 1)
	{
		//bossaren bizitza 0 denean, hil egingo da
		if (bizitzaBoss == 0)
		{
			pertsonaiaMarraztuPB();
		}
		erasoaY = 600;
	}
}

void bossaMarraztuPB()
{
	if (bizitzaBoss > 0)
	{
		if (BossPosturaP == 0)
		{
			kargatuBossa(".\\Argazkiak\\Program-bmp\\Etsaiak\\Enaitz.bmp");
		}
		else
		{
			kargatuBossa(".\\Argazkiak\\Program-bmp\\Etsaiak\\Enaitz.bmp");
		}

		SDL_Rect dstrect;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 194;
		dstrect.h = 179;

		//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
	else
	{
		if (bossX != 1205)
		{
			hilPB(2);
			SDL_Delay(500);
			TxanponaMugituPB();
		}

		kargatuBossa(".\\Argazkiak\\Lobby-bmp\\Teleport.bmp"); //etsai nagusia hlitzen denean, ate beltza agertu

		SDL_Rect dstrect;

		bossY = 447 - 173;
		bossX = 1205;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 90;
		dstrect.h = 173;

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
}

void TxanponaLortuPB()
{
	mapaMarraztuPB();

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaP);
	bihotzaMarraztu(bizitzaBoss);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	SDL_RenderPresent(Render);

	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\txanponak.bmp");

	dstrect.x = 565;
	dstrect.y = TxanponaY;
	dstrect.w = 75;
	dstrect.h = 75;

	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void TxanponaMugituPB()
{
	TxanponaY = 300;
	int i;

	for (i = 1; i <= 100; i++) //txanpona agertzerakoan, gorantz joango da. 
	{
		TxanponaY--;
		TxanponaLortuPB();
	}

	txanpona++;

}

void jausiPB() 
{
	KolisioaDetektatuPB(pertsonaiaX, pertsonaiaY + 5);

	while (kolisioa == 0)
	{
		pertsonaiaY += 5;
		pertsonaiaMarraztuPB();

		KolisioaDetektatuPB(pertsonaiaX, pertsonaiaY + 5);
	}
}

void erasoaMarraztuPB(int X)
{
	if (bizitzaBoss > 0)
	{
		SDL_Rect dstrect;

		dstrect.x = X - mapaX;
		dstrect.y = erasoaY;
		dstrect.w = 70;
		dstrect.h = 160;

		SDL_RenderCopy(Render, ErasoaT, NULL, &dstrect);
	}
}

void erasoaMugituPB()
{
	int i;
	
	KolisioaDetektatuPB(pertsonaiaX, pertsonaiaY);

	//pertsonaia eta boss-a bizirik daudenean, atakeak jarraitu
	if ((bizitzaBoss > 0) && (erasoaY < 600))
	{
		if (erasoaY == -160)
		{
			T0 = SDL_GetTicks64();
		}

		T = SDL_GetTicks64();

		erasoaY = (T - T0) / 3;

		pertsonaiaMarraztuPB();

		for (i = 0; i <= 5; i++)
		{
			if (i == Non579)
			{
				if (Zein579 == 0)
				{
					kargatuErasoa(".\\Argazkiak\\Program-bmp\\9.bmp");
					erasoaMarraztuPB(erasoaX + i * 200);
				}
				else if (Zein579 == 1)
				{
					kargatuErasoa(".\\Argazkiak\\Program-bmp\\7.bmp");
					erasoaMarraztuPB(erasoaX + i * 200);
				}
				else if (Zein579 == 2)
				{
					kargatuErasoa(".\\Argazkiak\\Program-bmp\\9.bmp");
					erasoaMarraztuPB(erasoaX + i * 200);
				}
				kargatuErasoa(".\\Argazkiak\\Program-bmp\\zero.bmp");
			}
			else
			{
				erasoaMarraztuPB(erasoaX + i * 200);
			}
		}

		SDL_RenderPresent(Render);
	}
}

int randomizatuPB(int noraino)
{
	return rand() % noraino;
}

void hilPB(int nor)
{
	while ((pertsonaiaY < 600) && (bossY < 600))
	{
		mapaMarraztuPB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaP);

		if (nor == 1)
		{
			bossaMarraztuPB();
			pertsonaiaY += 7;
		}
		else
		{
			bossY += 7;

			SDL_Rect dstrect;

			dstrect.x = bossX - mapaX;
			dstrect.y = bossY;
			dstrect.w = 194;
			dstrect.h = 179;

			SDL_RenderCopy(Render, BossT, NULL, &dstrect);
		}

		SDL_RenderPresent(Render);
	}
}

int Mediakalkulatu()
{
	int Media;

	Media = bizitzaR + bizitzaO + bizitzaM + bizitzaF + bizitzaP;
	Media = Media / 5;

	return Media;
}

void bukaerakoNotaMarraztu(int media)
{
	kargatuIrudia(".\\Argazkiak\\PuntuazioPantaila.bmp");

	SDL_RenderCopy(Render, irudiaT, NULL, NULL);

	bizitzaKargatu(media);

	SDL_Rect NotaRect;

	NotaRect.y = 0;
	NotaRect.w = 85;
	NotaRect.h = 75;

	SDL_Rect Dest;

	Dest.x = 580;
	Dest.y = 385;
	Dest.w = 85;
	Dest.h = 75;

	if (media == 0)
	{
		NotaRect.x = 225;
	}
	else if (media == 1)
	{
		NotaRect.x = 300;
	}
	else if (media == 2)
	{
		NotaRect.x = 375;
	}
	else if (media == 3)
	{
		NotaRect.x = 440;
	}
	else if (media == 4)
	{
		NotaRect.x = 510;
	}
	else if (media == 5)
	{
		NotaRect.x = 580;
	}
	else if (media == 6)
	{
		NotaRect.x = 660;
	}
	else if (media == 7)
	{
		NotaRect.x = 740;
	}
	else if (media == 8)
	{
		NotaRect.x = 810;
	}
	else if (media == 9)
	{
		NotaRect.x = 890;
	}
	else
	{
		NotaRect.x = 983;
	}

	SDL_RenderCopy(Render, Nota, &NotaRect, &Dest);
	SDL_RenderPresent(Render);
	SDL_Delay(5000);
}