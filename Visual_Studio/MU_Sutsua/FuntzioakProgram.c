#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakLobby.h"
#include "FuntzioakProgramBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakProgram.h"


int norabidea;

void mainProgram()
{
	//aldagai globalen deklarazioa
	pertsonaiaX = 120;
	pertsonaiaY = 464 - 125;
	mapaX = 0;

	pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten)) //bukle nagusia. bukle hau jokoa martxan dagoen bitartean ez da geldituko. Mugimendu sistema
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuP(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					PertsonaiaEzkerreraP();
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraP();
					break;
				}
			}
		}
	}

	if (irten == 2)
	{
		irten = 0;
		if (bizitzaP > 0)
		{
			bizitzaP--;
			SDL_Delay(500);
			mainProgram();
		}
		else
		{
			SDL_Delay(500);
			mainLobby();
		}
	}
	else if (irten == 3)
	{
		irten = 0;
		SDL_Delay(500);
		mainProgramBoss();
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaP = 0;
		mainLobby();
	}
	else
	{
		bizitzaP = 0;
	}
}

void pertsonaiaMarraztuP(char helbidea[])
{
	mapaMarraztuP(); //mapa marraztu

	kargatuPertsonaia(helbidea); //pertsonaia marraztu

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaP);  //pertsonaiaren bizitza marraztu
	//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);

	//EtsaienMugimendua();
}

void mapaMarraztuP()
{
	//mapa bi marrazkitan banatua dago
	if (mapaX + pertsonaiaX <= 5000)
	{
		kargatuIrudia(".\\Argazkiak\\Program-bmp\\ProgramNibela1.bmp");//maparen lehen zatia marraztu

		SDL_Rect mapa;

		mapa.x = mapaX;
		mapa.y = 0;
		mapa.w = 1200;
		mapa.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	}
	else
	{
		kargatuIrudia(".\\Argazkiak\\Program-bmp\\ProgramNibela2.bmp"); //maparen bigarren zatia marraztu

		SDL_Rect mapa2;

		mapa2.x = mapaX - 4000;
		mapa2.y = 0;
		mapa2.w = 1200;
		mapa2.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa2, NULL);
	}

	/*SDL_Rect dstRect;

	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = 1200;
	dstRect.h = 600;*/

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);
}

void PertsonaiaEzkerreraP()
{
	kolisioaDetektatuP(pertsonaiaX - 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik


	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");//pertsonaia maparen erdida heltzen denean, mapa mugituko da
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 2;
		jausiP();
	}
}
void PertsonaiaEskumaraP()
{
	kolisioaDetektatuP(pertsonaiaX + 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))//pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			pertsonaiaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 1;
	}
	jausiP();
}

void PertsonaiaEzkerreraSaltoP(int Y)
{
	kolisioaDetektatuP(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			mapaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaX -= 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		norabidea = 2;
	}
}
void PertsonaiaEskumaraSaltoP(int Y)
{
	kolisioaDetektatuP(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 10;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			mapaX += 10;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaX += 15;
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		norabidea = 1;
	}
}

void kolisioaDetektatuP(int x, int y)
{
	//mapan dauden kolisio guztiak kalkulatzen ditu. Horrela mapari sentzua emanez.
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 10000))
	{
		kolisioa = 1;
	}
	else if (y - 125 > 600)
	{
		kolisioa = 1;
	}
	else if ((y > 464) && (x < 239))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 239) && (x < 320))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x > 320) && (x < 400))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x > 400) && (x < 479))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x > 560) && (x < 799))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x > 880) && (x < 959))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x > 1040) && (x < 1120))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x > 1120) && (x < 1297))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x > 1360) && (x < 1439))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x > 1439) && (x < 1599))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 1599) && (x < 1839))
	{
		kolisioa = 1;
	}
	else if ((y > 464) && (x > 1839) && (x < 2608))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 2608) && (x < 2688))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x > 2688) && (x < 2848))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x > 2848) && (x - 30 < 2927))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 3008) && (x - 30 < 3087))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 3168) && (x - 30 < 3247))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x + 30 > 3328) && (x - 30 < 3407))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x + 30 > 3488) && (x - 30 < 3567))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 3648) && (x - 30 < 3727))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x + 30 > 3808) && (x - 30 < 3887))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 3968) && (x - 30 < 4047))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x + 30 > 4128) && (x - 30 < 4207))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x + 30 > 4288) && (x < 4447))
	{
		kolisioa = 1;
	}
	else if ((y > 344) && (x > 4447) && (x < 4567))
	{
		kolisioa = 1;
	}
	else if ((y > 424) && (x > 4567) && (x < 4727))
	{
		kolisioa = 1;
	}
	else if ((y > 463) && (x > 4727) && (x < 5439))
	{
		kolisioa = 1;
	}
	else if ((y > 388) && (x > 5439) && (x < 5519))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x > 5519) && (x < 5678))
	{
		kolisioa = 1;
	}
	else if ((y > 388) && (x > 5678) && (x < 5758))
	{
		kolisioa = 1;
	}
	else if ((y > 464) && (x > 5758) && (x < 7008))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 7008) && (x - 30 < 7087))
	{
		kolisioa = 1;
	}
	else if ((y > 346) && (x + 30 > 7152) && (x - 30 < 7231))
	{
		kolisioa = 1;
	}
	else if ((y > 306) && (x + 30 > 7296) && (x - 30 < 7455))
	{
		kolisioa = 1;
	}
	else if ((y > 266) && (x + 30 > 7520) && (x - 30 < 7599))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 7664) && (x - 30 < 7823))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 7904) && (x - 30 < 8063))
	{
		kolisioa = 1;
	}
	else if ((y > 224) && (x + 30 > 8144) && (x - 30 < 8223))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x + 30 > 8288) && (x - 30 < 8367))
	{
		kolisioa = 1;
	}
	else if ((y > 344) && (x > 8432) && (x < 8511))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 8511) && (x < 8672))
	{
		kolisioa = 1;
	}
	else if ((y > 344) && (x > 8672) && (x < 8751))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 8751) && (x < 8912))
	{
		kolisioa = 1;
	}
	else if ((y > 344) && (x > 8912) && (x < 8991))
	{
		kolisioa = 1;
	}
	else if ((y > 464) && (x > 8991) && (x < 10000))
	{
		kolisioa = 1;
	}
	else if ((y > 453) && (x > 9600) && (x < 9680))
	{
		irten = 3;
	}

}

void SaltoaMarraztuP(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT://bi funtzio hauen eta goiko bi funtzio haien diferentzia nagusia, hanken mugimendua da. Funtzio hauetan, hankak ez dira mugitzen ezkerrera edo eskumara egitean.
					PertsonaiaEzkerreraSaltoP(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoP(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuP(pertsonaiaX, Y);

	if (kolisioa == 0)
	{
		pertsonaiaY = Y;
		if (norabidea == 2)
		{
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuP(t0, y0, vy);
	}
	else
	{
		jausiP();
	}
}
void jausiP()
{
	kolisioaDetektatuP(pertsonaiaX, pertsonaiaY + 7);

	while (kolisioa == 0)//kolisioa ez dagoen bitartean, lurra ez dagoen bitartean, pertsonaia jausi egingo da.
	{
		pertsonaiaY += 7;
		if (norabidea == 1)
		{
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuP(pertsonaiaX, pertsonaiaY + 7);
	}

	if (pertsonaiaY >= 550)// hemen sartzen bada, pertsonaia hutsunera jausi egin da.
	{
		irten = 2;
	}

	pertsonaiaY += 7;
	kolisioaDetektatuP(pertsonaiaX, pertsonaiaY);
	while (kolisioa == 1)
	{
		kolisioaDetektatuP(pertsonaiaX, pertsonaiaY - 1);
		pertsonaiaY -= 1;
	}

	if (norabidea == 1)
	{
		pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuP(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
}