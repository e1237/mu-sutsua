#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakOinarriBoss.h"
#include "FuntzioakLobby.h"
#include "FuntzioakSDL.h"

int NorabideaOB = 1;
int norabideaPunch;
int mugimenduKop = 0;
int bossImg;

void mainOinarriBoss()
{
	pertsonaiaX = 180;
	pertsonaiaY = 350 - 125;
	mapaX = 100;
	mugimenduKop = 0;
	norabideaPunch = 0;
	bossImg = 1;

	bizitzaBoss = 3;

	bossX = 1100;
	bossY = 350 - 228;

	erasoaX = 1100 - 175;
	erasoaY = 170;

	pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten))
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuOB(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					EzkerreraOB();
					break;
				case SDLK_RIGHT:
					EskumaraOB();
					break;
				}

				if (bizitzaBoss > 0)
				{
					if ((mugimenduKop == 10) && (norabideaPunch == 0))
					{
						norabideaPunch = 1;
						bossImg = 2;
					}
					else
					{
						mugimenduKop++;
					}
				}
			}
		}

		if ((norabideaPunch != 0) && (bizitzaBoss > 0))
		{
			erasoaMugituOB();
		}
	}

	if (irten == 2)
	{
		irten = 0;
		if (bizitzaBoss == 0)
		{
			SDL_Delay(500);
			if (nibela == 1)
			{
				nibela++;
			}
			ureztatu = 1;
			mainLobby();
		}
		else
		{
			SDL_Delay(1000);
			ureztatu = 1;
			mainLobby();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaO = 0;
		mainLobby();
	}
	else
	{
		bizitzaO = 0;
	}
}

void pertsonaiaMarraztuOB(char helbidea[])
{
	if (bizitzaO > 0)
	{
		mapaMarraztuOB();

		kargatuPertsonaia(helbidea);

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		bossaMarraztuOB();

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaO);
		bihotzaMarraztu(bizitzaBoss);

		SDL_RenderPresent(Render);

		if (bossImg == 3)
		{
			SDL_Delay(500);
			bossImg = 1;
			pertsonaiaMarraztuOB(helbidea);
		}

		/*if (mugimenduKop == 10)
		{
			norabideaPunch = 1;
			mugimenduKop++;
		}
		else
		{
			mugimenduKop++;
		}*/
	}
	else
	{
		hil(1);
		irten = 2;
	}
}

void bossaMarraztuOB()
{
	if (bizitzaBoss > 0)
	{
		if (bossImg == 1)
		{
			kargatuBossa(".\\Argazkiak\\Oinarri-bmp\\Etsaiak\\Aitor.bmp");
		}
		else if (bossImg == 2)
		{
			kargatuBossa(".\\Argazkiak\\Oinarri-bmp\\Etsaiak\\Aitor-punch.bmp");
		}
		else
		{
			kargatuBossa(".\\Argazkiak\\Oinarri-bmp\\Etsaiak\\AitorOuch.bmp");
		}

		SDL_Rect dstrect;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 180;
		dstrect.h = 228;

		//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
	else
	{
		if (bossX != 1150)
		{
			hil(2);
			SDL_Delay(500);
			TxanponaMugituOB();
		}

		kargatuBossa(".\\Argazkiak\\Lobby-bmp\\Teleport.bmp");

		SDL_Rect dstrect;

		bossY = 180;
		bossX = 1150;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 90;
		dstrect.h = 173;

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
}

void TxanponaLortuOB()
{
	mapaMarraztuOB();

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaO);
	bihotzaMarraztu(bizitzaBoss);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	SDL_RenderPresent(Render);

	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\txanponak.bmp");

	dstrect.x = 565;
	dstrect.y = TxanponaY;
	dstrect.w = 75;
	dstrect.h = 75;


	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void TxanponaMugituOB()
{
	TxanponaY = 300;
	int i;

	for (i = 1; i <= 100; i++)
	{
		TxanponaY--;
		TxanponaLortuOB();
	}

	txanpona++;
}

void erasoaMarraztuOB(char helbidea[])
{
	if (bizitzaBoss > 0)
	{
		kargatuErasoa(helbidea);

		SDL_Rect dstrect;

		dstrect.x = erasoaX - mapaX;
		dstrect.y = erasoaY;
		dstrect.w = 198;
		dstrect.h = 102;

		//SDL_BlitSurface(Erasoa, NULL, Superficie, &dstrect);

		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, ErasoaT, NULL, &dstrect);
		SDL_RenderPresent(Render);
	}
}

void erasoaMugituOB()
{
	kolisioaDetektatuOB(pertsonaiaX, pertsonaiaY);
	if (norabideaPunch == 1)
	{
		erasoaX -= 10;
		erasoaMarraztuOB(".\\Argazkiak\\Oinarri-bmp\\Etsaiak\\Punch.bmp");
	}
	else if (norabideaPunch == 2)
	{
		erasoaX += 10;
		erasoaMarraztuOB(".\\Argazkiak\\Oinarri-bmp\\Etsaiak\\Punch2.bmp");
	}

	if ((erasoaX <= 0) || (erasoaX + 100 >= bossX) || (kolisioa == 3))
	{
		if (erasoaX + 100 >= bossX)
		{
			bossImg = 3;
		}
		else
		{
			bossImg = 1;
		}

		if (kolisioa == 3)
		{
			bizitzaO--;
		}
		else if ((erasoaX + 100 >= bossX) && (norabideaPunch == 2))
		{
			bizitzaBoss--;
			if (bizitzaO < 10)
			{
				bizitzaO++;
			}
		}
		mugimenduKop = 0;
		norabideaPunch = 0;
		erasoaX = 1100 - 175;
	}

	if (NorabideaOB == 1)
	{
		pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
}

void mapaMarraztuOB()
{
	kargatuIrudia(".\\Argazkiak\\Oinarri-bmp\\Arena_lucha.bmp");

	SDL_Rect mapa;

	mapa.x = mapaX;
	mapa.y = 0;
	mapa.w = 1200;
	mapa.h = 600;

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
}

void EzkerreraOB()
{
	kolisioaDetektatuOB(pertsonaiaX - 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 700) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}

	NorabideaOB = 1;
}

void EskumaraOB()
{
	kolisioaDetektatuOB(pertsonaiaX + 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 700) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
	}

	NorabideaOB = 2;
}

void PertsonaiaEzkerreraSaltoOB(int Y)
{
	kolisioaDetektatuOB(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 700) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
		}

		NorabideaOB = 1;
	}
}

void PertsonaiaEskumaraSaltoOB(int Y)
{
	kolisioaDetektatuOB(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 700) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
		}
		NorabideaOB = 2;
	}
}

void kolisioaDetektatuOB(int x, int y)
{
	y += 125;
	x += mapaX + 35;

	if ((x - 35 <= 135) || (x + 35 >= 1365))
	{
		kolisioa = 1;
	}
	else if (y > 350)
	{
		kolisioa = 1;
	}
	else if ((x >= erasoaX) && (x <= erasoaX + 198) && (y >= erasoaY) && (norabideaPunch != 0))
	{
		if (y >= erasoaY + 10)
		{
			kolisioa = 3;
		}
		else
		{
			kolisioa = 2;
		}
	}
	else if ((bizitzaBoss > 0) && (x >= bossX) && (x <= bossX + 180) && (y > bossY))
	{
		kolisioa = 1;
	}
	else if ((bizitzaBoss == 0) && (x - 35 >= bossX) && (x <= bossX + 90) && (y > bossY))
	{
		irten = 2;
	}
	else
	{
		kolisioa = 0;
	}
}

void SaltoaMarraztuOB(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	if (norabideaPunch != 0)
	{
		erasoaMugituOB();
	}

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					PertsonaiaEzkerreraSaltoOB(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoOB(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuOB(pertsonaiaX, Y);

	if (kolisioa == 0) {
		pertsonaiaY = Y;
		if (NorabideaOB == 1)
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuOB(t0, y0, vy);
	}
	else if ((kolisioa == 2) || (kolisioa == 3)) {

		if (NorabideaOB == 1)
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		t0 = SDL_GetTicks64();

		norabideaPunch = 2;

		SaltoaMarraztuOB(t0, pertsonaiaY, 40);
	}
	else
	{
		jausiOB();
	}
}

void jausiOB()
{
	kolisioaDetektatuOB(pertsonaiaX, pertsonaiaY + 5);

	while (kolisioa == 0)
	{
		pertsonaiaY += 5;
		if (NorabideaOB == 1)
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuOB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuOB(pertsonaiaX, pertsonaiaY + 5);
	}
}

void hil(int nor)
{
	while ((pertsonaiaY < 600) && (bossY < 600))
	{
		mapaMarraztuOB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaO);

		if (nor == 1)
		{
			bossaMarraztuOB();
			pertsonaiaY += 7;
		}
		else
		{
			bossY += 7;

			SDL_Rect dstrect;

			dstrect.x = bossX - mapaX;
			dstrect.y = bossY;
			dstrect.w = 180;
			dstrect.h = 228;

			//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
			//SDL_UpdateWindowSurface(screen);

			SDL_RenderCopy(Render, BossT, NULL, &dstrect);
		}

		SDL_RenderPresent(Render);
	}
}
