#ifndef FUNTZIOAKPROGRAMBOSS_H
#define FUNTZIOAKPROGRAMBOSS_H

void mainProgramBoss();

void mapaMarraztuPB();
void pertsonaiaMarraztuPB();

void EzkerreraPB();
void EskumaraPB();

void SaltoaMarraztuPB(int t0, int y0, int vy);
void PertsonaiaEzkerreraSaltoPB(int Y);
void PertsonaiaEskumaraSaltoPB(int Y);

void KolisioaDetektatuPB(int x, int y);
void erasoarekinKolisioaPB(int x, int y);

void bossaMarraztuPB();
void TxanponaLortuPB();
void TxanponaMugituPB();

void jausiPB();

void erasoaMarraztuPB(int X);
void erasoaMugituPB();

int randomizatuPB(int noraino);
void hilPB(int nor);

int Mediakalkulatu();
void bukaerakoNotaMarraztu(int media);

#endif