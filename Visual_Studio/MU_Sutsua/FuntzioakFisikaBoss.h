#ifndef FUNTZIOAKFISIKABOSS_H
#define FUNTZIOAKFISIKABOSS_H

void mainFisikaBoss();

void pertsonaiaMarraztuFB(char helbidea[]);
void mapaMarraztuFB();

void EzkerreraFB();
void EskumaraFB();
void SaltoaMarraztuFB(int t0, int y0, int vy);
void PertsonaiaEzkerreraSaltoFB(int Y);
void PertsonaiaEskumaraSaltoFB(int Y);
void jausiFB();

void bossaMarraztuFB();
void hilF(int nor);
void TxanponaLortuFB();
void TxanponaMugituFB();
void erasoaMarraztuFB(char helbidea[]);
void erasoaMugituFB();

int randomizatuX();

void kolisioaDetektatuFB(int x, int y);
void erasoarekinKolisioaFB(int x, int y);

#endif