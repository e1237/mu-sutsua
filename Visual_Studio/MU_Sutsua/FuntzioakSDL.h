#ifndef FUNTZIOAKSDL_H
#define FUNTZIOAKSDL_H

#include <SDL_mixer.h>

int Nibela, nibela, kolisioa, ureztatu;
int kutxaIrekita, rekuEginda;
int hasiera, kendu, irten;
float T0, T;
int bizitzaBoss;
int apunteKop, txanpona;
int pertsonaiaX, pertsonaiaY, mapaX;
int EtsaiakX, EtsaiakY, bossX, bossY, XabiY, TxanponaY;
int erasoaX, erasoaY;
int bizitzaR, bizitzaO, bizitzaM, bizitzaF, bizitzaP;

SDL_Surface* irudia;
SDL_Surface* Superficie;

SDL_Window* screen;
SDL_Renderer* Render;

SDL_Texture* pertsonaiaT;
SDL_Texture* irudiaT;
SDL_Texture* EtsaiakT;
SDL_Texture* BossT;
SDL_Texture* ErasoaT;
SDL_Texture* Nota;
SDL_Texture* kargaPantaila;
SDL_Texture* Objektua;
SDL_Texture* Kutxa;
SDL_Texture* Errepikatzailea;
SDL_Texture* Bihotza;

int Hasieratu();
void itxi();

void kargatuIrudia(char helbidea[]);
void kargatuPertsonaia(char helbidea[]);
void kargatuEtsaiak(char helbidea[]);
void kargatuBossa(char helbidea[]);
void kargatuErasoa(char helbidea[]);
void kargatuKargaPantaila(char helbidea[]);
void kargatuObjektua(char helbidea[]);
void kargatuKutxa(char helbidea[]);
void kargatuErrepikatzailea(char helbidea[]);

void bizitzaKargatu(int bizitza);
void bizitzaMarraztu(int bizitza);

void bihotzaKargatu(int bizitza);
void bihotzaMarraztu(int bizitza);

void partidaIrakurri();
void partidaIdatzi();

void audioInit();
int loadTheMusic(char* fileName);
int playMusic(void);
void toggleMusic(void);
void musicUnload(void);
void audioTerminate();

void KargaPantailaMarraztu(char helbidea[]);
void KargaPantaila();
void Partidak();

#endif