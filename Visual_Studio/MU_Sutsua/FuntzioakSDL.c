#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakSDL.h"

Mix_Music* musika = NULL;

int Hasieratu()
{
	int zuzena = 1;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		printf("ERROREA: Ezin izan da SDL hasieratu, Error SDL: %s\n", SDL_GetError());
		zuzena = 0;
		// Hasieratu SDL guztia
	}
	else {
		screen = SDL_CreateWindow("MU Sutsua", 50, 50, 1200, 600, SDL_WINDOW_SHOWN);
		// Sortu pantaila
		if (screen == NULL) {
			printf("ERROREA: Ezin izan da leihoa sortu, SDL_Error: %s\n", SDL_GetError());
			zuzena = 0;
		
		}
		else {
			Render = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);
			Superficie = SDL_GetWindowSurface(screen);
			// Render-a eta gainazala sortu
		}
	}
	

	return zuzena;
}

void itxi()
{
	/*SDL_FreeSurface(irudia);
	SDL_FreeSurface(pertsonaia);
	SDL_FreeSurface(Superficie);
	SDL_FreeSurface(Etsaiak);
	SDL_FreeSurface(Boss);

	irudia = NULL;
	pertsonaia = NULL;
	Superficie = NULL;
	Etsaiak = NULL;
	Boss = NULL;*/
	partidaIdatzi();

	SDL_DestroyTexture(pertsonaiaT);
	SDL_DestroyTexture(irudiaT);
	SDL_DestroyTexture(BossT);
	SDL_DestroyTexture(ErasoaT);
	SDL_DestroyTexture(EtsaiakT);
	SDL_DestroyRenderer(Render);

	// Testurak suntsitu

	toggleMusic();
	audioTerminate();

	// Musika amaitu

	SDL_DestroyWindow(screen);
	// Leihoa suntsitu

	SDL_Quit();
	//SDL itxi
}

void kargatuIrudia(char helbidea[]) {

	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(irudiaT);
	irudiaT = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL) {
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	SDL_RenderClear(Render);
	irudiaT = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	// Render garbitu, gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuPertsonaia(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(pertsonaiaT);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL) {
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	pertsonaiaT = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuBossa(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(BossT);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL) {
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	BossT = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuEtsaiak(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(EtsaiakT);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	EtsaiakT = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuErasoa(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(ErasoaT);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	ErasoaT = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuKargaPantaila(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(kargaPantaila);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	kargaPantaila = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuObjektua(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(Objektua);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	Objektua = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuKutxa(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(Kutxa);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	Kutxa = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void kargatuErrepikatzailea(char helbidea[])
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(Errepikatzailea);
	irudia = NULL;

	irudia = SDL_LoadBMP(helbidea);
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", helbidea, SDL_GetError());
		// Kargatu irudia
	}

	Errepikatzailea = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void bizitzaKargatu(int bizitza)
{
	SDL_DestroyTexture(Nota);
	//SDL_FreeSurface(irudia);

	if (bizitza >= 5) 
	{
		irudia = SDL_LoadBMP(".\\Argazkiak\\Pertsonaiak-bmp\\Bizitza.bmp");
		// Bizitza 5-etik gora baldin bada nota berdez agertu
		
	}
	else
	{
		irudia = SDL_LoadBMP(".\\Argazkiak\\Pertsonaiak-bmp\\Bizitza2.bmp");
		// Bizitza 5-etik behera baldin bada nota gorriz agertu
		
	}

	Nota = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void bizitzaMarraztu(int bizitza)
{
	bizitzaKargatu(bizitza);
	// bizitza Kargatu

	SDL_Rect NotaRect;

	NotaRect.x = 0;
	NotaRect.y = 0;
	NotaRect.w = 200;
	NotaRect.h = 75;

	SDL_Rect Dest;

	Dest.x = 10;
	Dest.y = 0;
	Dest.w = 200;
	Dest.h = 75;

	SDL_RenderCopy(Render, Nota, &NotaRect, &Dest);

	if (bizitza == 0)
	{
		NotaRect.x = 225;
	}
	else if (bizitza == 1)
	{
		NotaRect.x = 300;
	}
	else if (bizitza == 2)
	{
		NotaRect.x = 375;
	}
	else if (bizitza == 3)
	{
		NotaRect.x = 440;
	}
	else if (bizitza == 4)
	{
		NotaRect.x = 510;
	}
	else if (bizitza == 5)
	{
		NotaRect.x = 580;
	}
	else if (bizitza == 6)
	{
		NotaRect.x = 660;
	}
	else if (bizitza == 7)
	{
		NotaRect.x = 740;
	}
	else if (bizitza == 8)
	{
		NotaRect.x = 810;
	}
	else if (bizitza == 9)
	{
		NotaRect.x = 890;
	}
	else
	{
		NotaRect.x = 983;
	}

	// Bizitzaren arabera irudiaren zati bat marraztu

	NotaRect.y = 0;
	NotaRect.w = 85;
	NotaRect.h = 75;

	Dest.x = 225;
	Dest.y = 0;
	Dest.w = 85;
	Dest.h = 75;

	SDL_RenderCopy(Render, Nota, &NotaRect, &Dest);
}

void bihotzaKargatu(int bizitza)
{
	//SDL_FreeSurface(irudia);
	SDL_DestroyTexture(Bihotza);


	irudia = SDL_LoadBMP(".\\Argazkiak\\Pertsonaiak-bmp\\Bihotza.bmp");
	// Kargatu BMP irudia

	if (irudia == NULL)
	{
		printf("Ezin izan da %s irudia kargatu SDL Error: %s\n", ".\\Argazkiak\\Pertsonaiak-bmp\\Bihotza.bmp", SDL_GetError());
		// Kargatu irudia
	}

	Bihotza = SDL_CreateTextureFromSurface(Render, irudia);
	SDL_FreeSurface(irudia);
	//Gainazaletik textura sortu eta amaitzeko gainazala ezabatu
}

void bihotzaMarraztu(int bizitza)
{
	if (bizitza != 0) // bizitza 0 ez bada bihotza marraztu
	{
		bihotzaKargatu(bizitza);

		SDL_Rect Dest;

		Dest.x = 1115;
		Dest.y = 10;
		Dest.w = 75;
		Dest.h = 66;

		SDL_RenderCopy(Render, Bihotza, NULL, &Dest);

		if (bizitza >= 2)
		{
			Dest.x = 1015;

			SDL_RenderCopy(Render, Bihotza, NULL, &Dest);
		}
		if (bizitza == 3)
		{
			Dest.x = 915;

			SDL_RenderCopy(Render, Bihotza, NULL, &Dest);
		}
	}
	// bizitzaren arabera bihotz kopurua marraztu
}

void partidaIdatzi() // Partida gorde
{
	FILE* fitx_erakuslea;
	fitx_erakuslea = fopen("Partida.txt", "w");

	fprintf(fitx_erakuslea, "%d %d %d %d %d %d", nibela, bizitzaR, bizitzaO, bizitzaM, bizitzaF, bizitzaP);

	fclose(fitx_erakuslea);
}

void partidaIrakurri() // Gordetako partida irakurri
{
	FILE* fitx_erakuslea;
	fitx_erakuslea = fopen("Partida.txt", "r");

	fscanf(fitx_erakuslea, "%d %d %d %d %d %d", &nibela, &bizitzaR, &bizitzaO, &bizitzaM, &bizitzaF, &bizitzaP);

	fclose(fitx_erakuslea);
}

void audioInit() // Musika hasieratu
{
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
	}
}

int loadTheMusic(char* fileName) // Musika kargatu
{
	int wasPlaying = 0;

	if (musika != NULL)
	{
		wasPlaying = (Mix_PlayingMusic() != 1);
		Mix_HaltMusic();
		//Mix_FreeMusic(musika);
	}
	if ((musika = Mix_LoadMUS(fileName)) == NULL) return 0;
	if (wasPlaying)  Mix_PlayMusic(musika, -1);
	return 1;
}

int playMusic(void) // Musika erreproduzitu
{
	if (musika != NULL)
	{
		Mix_PlayMusic(musika, -1);
		return 1;
	}
	return 0;
}

void toggleMusic(void) // Musika erreproduzitu/gelditu
{
	if (musika != NULL)
	{
		if (Mix_PlayingMusic() != 0)
		{
			if (Mix_PausedMusic()) Mix_ResumeMusic();
			else Mix_PauseMusic();
		}
		else playMusic();
	}
}

void musicUnload(void) // Musika gelditu
{
	if (musika != NULL)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(musika);
	}
}

void audioTerminate(void) // Musika kendu
{
	Mix_HaltChannel(-1);
	musicUnload();
	Mix_Quit();
}

void KargaPantailaMarraztu(char helbidea[]) 
{
	kargatuIrudia(helbidea);

	SDL_RenderCopy(Render, irudiaT, NULL, NULL);
	SDL_RenderPresent(Render);
}

void KargaPantaila()
{
	KargaPantailaMarraztu(".\\Argazkiak\\KargaPantaila.bmp");

	int x = 0, y = 0;

	SDL_Event event;

	if (SDL_PollEvent(&event) != 0)
	{
		if (event.type == SDL_MOUSEBUTTONUP)
		{
			SDL_GetMouseState(&x, &y);
		}
		else if (event.type == SDL_QUIT) {
			kendu = 1;
		}
	}
	
	// Klick egitean saguaren x eta y posizioa jaso
	if (kendu != 1)
	{
		if ((x > 462 && x < 623) && (y > 368 && y < 432))
		{
			Partidak();
			// Jokoa hasteko botoia jotzen badu partida pantilara pasatu
		}
		else
		{
			KargaPantaila(); // Bestela berriro jaso x eta y posizioa
		}
	}
}

void Partidak()
{
	KargaPantailaMarraztu("Argazkiak\\Lobby-bmp\\PartidakDefinitivo.bmp");

	int x = 0, y = 0;
	
	SDL_Event event;

	if (SDL_PollEvent(&event) != 0)
	{
		if (event.type == SDL_MOUSEBUTTONUP)
		{
			SDL_GetMouseState(&x, &y);
		}
		else if (event.type == SDL_QUIT) {
			kendu = 1;
		}
	}

	// Klick egitean saguaren x eta y posizioa jaso
	if (kendu != 1)
	{
		if ((x > 292 && x < 878) && (y > 127 && y < 254))
		{
			hasiera = 1;
			nibela = 0;
			bizitzaR = 5;
			bizitzaO = 5;
			bizitzaM = 5;
			bizitzaF = 5;
			bizitzaP = 0;
			mainLobby();
			// Partida berria hasten bada aldagai guztiak hasieratu
		}
		else if ((x > 292 && x < 878) && (y > 346 && y < 473))
		{
			hasiera = 0;
			partidaIrakurri();
			mainLobby();
			// Partida kargatu jotzen badu aurretik dauden datuak kargatu
		}
		else
		{
			Partidak();
		}
	}
}