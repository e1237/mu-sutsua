#ifndef FUNTZIOAKOINARRIBOSS_H
#define FUNTZIOAKOINARRIBOSS_H

void mainOinarriBoss();

void pertsonaiaMarraztuOB(char helbidea[]);
void bossaMarraztuOB();
void TxanponaLortuOB();
void TxanponaMugituOB();

void erasoaMarraztuOB(char helbidea[]);
void erasoaMugituOB();

void mapaMarraztuOB();

void EzkerreraOB();
void EskumaraOB();

void PertsonaiaEzkerreraSaltoOB(int Y);
void PertsonaiaEskumaraSaltoOB(int Y);

void kolisioaDetektatuOB(int x, int y);

void SaltoaMarraztuOB(float t0, int y0, int vy);
void jausiOB();
void hil(int nor);

#endif