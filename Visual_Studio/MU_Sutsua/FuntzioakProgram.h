#ifndef FUNTZIOAKPROGRAM_H
#define FUNTZIOAKPROGRAM_H

void mainProgram();

void pertsonaiaMarraztuP(char helbidea[]);
void mapaMarraztuP();

void PertsonaiaEzkerreraP();
void PertsonaiaEskumaraP();

void PertsonaiaEzkerreraSaltoP(int Y);
void PertsonaiaEskumaraSaltoP(int Y);

void kolisioaDetektatuP(int x, int y);

void SaltoaMarraztuP(float t0, int y0, int vy);
void jausiP();
#endif