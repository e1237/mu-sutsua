#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakRedes.h"
#include "FuntzioakLobby.h"
#include "FuntzioakRedesBoss.h"
#include "FuntzioakSDL.h"

int norabidea, EtsaiaAukeratu, kolisioaEtsaiakPertsonaia, ikusi, posEtsaia;

void mainRedes()
{
	//aldagai globalen deklarazioa
	pertsonaiaX = 120;
	pertsonaiaY = 449 - 125;
	mapaX = 0;
	posEtsaia = 0;
	ikusi = 0;

	EtsaiakY = 449 - 102;

	pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");
	posiziozAldatuEtsaia();

	SDL_Event e;

	while ((!kendu) && (!irten)) //bukle nagusia. bukle hau jokoa martxan dagoen bitartean ez da geldituko. Mugimendu sistema
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuR(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					PertsonaiaEzkerrera();
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumara();
					break;
				}
			}
		}

		//EtsaiaIkustenAhalDa();

		KolisioaEtsaiakPertsonaia();
		//if (KolisioaEtsaiakPertsonaia() != 0 || ikusi != 1)
		if ((EtsaiakX >= mapaX) && (EtsaiakX <= mapaX + 1200) && (kolisioaEtsaiakPertsonaia == 0) && (ikusi == 0))
		{
			EtsaiaAukeratuRandom();
			//kokatuEtsaia();
			ikusi = 1;
		}
		else if (EtsaiakX < mapaX)
		{
			ikusi = 0;
			posEtsaia++;
			posiziozAldatuEtsaia();
		}
		else if (ikusi == 1)
		{
			EtsaiaMugitu();
		}
	}

	if ((irten == 2) && (bizitzaR == 0))
	{
		SDL_Delay(500);
		irten = 0;
		mainLobby();
	}
	else if ((bizitzaR > 0) && (irten == 2))
	{
		bizitzaR--;
		SDL_Delay(500);
		irten = 0;
		mainRedes();
	}
	else if (irten == 3)
	{
		SDL_Delay(500);
		irten = 0;
		mainRedesBoss();
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaR = 0;
		mainLobby();
	}
	else
	{
		bizitzaR = 0;
	}
}

void pertsonaiaMarraztuR(char helbidea[])
{
	mapaMarraztuR(); //mapa marraztu

	kargatuPertsonaia(helbidea);  //pertsonaia marraztu

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaR); //pertsonaiaren bizitza marraztu
	//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);

	//EtsaienMugimendua();
}

void mapaMarraztuR()
{
	//nibela bi marrazkitan banatuta dago
	if (mapaX + pertsonaiaX <= 5000)
	{
		kargatuIrudia(".\\Argazkiak\\Redes-bmp\\RedesNibela1.bmp");  //nibelaren lehen zatia marraztu

		SDL_Rect mapa;

		mapa.x = mapaX;
		mapa.y = 0;
		mapa.w = 1200;
		mapa.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	}
	else
	{
		kargatuIrudia(".\\Argazkiak\\Redes-bmp\\RedesNibela2.bmp"); //nibelaren bigarren zatia marraztu

		SDL_Rect mapa2;

		mapa2.x = mapaX - 4000;
		mapa2.y = 0;
		mapa2.w = 1200;
		mapa2.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa2, NULL);
	}

	/*SDL_Rect dstRect;

	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = 1200;
	dstRect.h = 600;*/

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);
}

void PertsonaiaEzkerrera()
{
	kolisioaDetektatuR(pertsonaiaX - 30, pertsonaiaY); //pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400)) //pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		else //hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 2;
		jausi();
	}
}

void PertsonaiaEskumara()
{
	kolisioaDetektatuR(pertsonaiaX + 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik


	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		else //hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			pertsonaiaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 1;
	}
	jausi();
}

void kolisioaDetektatuR(int x, int y)
{
	kolisioa = 0;
	//mapan dauden kolisio guztiak kalkulatzen ditu. Horrela mapari sentzua emanez.
	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 10000))
	{
		kolisioa = 1;
	}
	else if ((y >= 450) && (x < 2396))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x + 30 > 1286) && (x - 30 < 1360))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x + 30 > 1448) && (x - 30 < 1522))
	{
		kolisioa = 1;
	}
	else if ((y > 151) && (x + 30 > 1609) && (x - 30 < 1683))
	{
		kolisioa = 1;
	}
	else if ((x + 35 >= 2497) && (x - 35 <= 2617) && (y >= 380))
	{
		kolisioa = 1;
	}
	else if ((y - 125 > 600) && ((x > 2396) && (x + 35 < 2497) || (x - 35 > 2617) && (x < 2719)))
	{
		kolisioa = 1;
	}
	else if ((y - 125 > 600) && (x + 35 >= 2497) && (x - 35 <= 2617))
	{
		kolisioa = 1;
	}
	else if (y > 449 && x > 2719 && x < 4173)
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 4173) && (x < 4248))
	{
		kolisioa = 1;
	}
	else if ((y > 301) && (x > 4248) && (x < 4323))
	{
		kolisioa = 1;
	}
	else if ((y > 227) && (x > 4323) && (x < 4398))
	{
		kolisioa = 1;
	}
	else if (y > 153 && x > 4398 && x < 4471)
	{
		kolisioa = 1;
	}
	else if (y - 125 > 600 && x + 35 > 4471 && x - 35 < 4595)
	{
		kolisioa = 1;
	}
	else if (y > 154 && x > 4595 && x < 4669)
	{
		kolisioa = 1;
	}
	else if (y > 228 && x > 4669 && x < 4744)
	{
		kolisioa = 1;
	}
	else if (y > 302 && x > 4744 && x < 4819)
	{
		kolisioa = 1;
	}
	else if (y > 374 && x > 4819 && x < 4894)
	{
		kolisioa = 1;
	}
	else if (y > 449 && x > 4894 && x < 6026)
	{
		kolisioa = 1;
	}
	else if (y > 378 && x > 6026 && x < 6101)
	{
		kolisioa = 1;
	}
	else if (y > 304 && x > 6101 && x < 6691)
	{
		kolisioa = 1;
	}
	else if (y > 378 && x > 6691 && x < 6766)
	{
		kolisioa = 1;
	}
	else if (y > 449 && x > 6766 && x < 9426)
	{
		kolisioa = 1;
	}
	else if (y > 212 && y < 282 + 125 && x > 7470 && x < 7844)
	{
		kolisioa = 1;
	}
	else if (y < 282 + 125 && x > 7844 && x - 35 < 7993)
	{
		kolisioa = 1;
	}
	else if (y < 71 + 125 && x > 8382 && x < 8457)
	{
		kolisioa = 1;
	}
	else if (y < 145 + 125 && x > 8457 && x < 8532)
	{
		kolisioa = 1;
	}
	else if (y < 219 + 125 && x > 8532 && x < 8607)
	{
		kolisioa = 1;
	}
	else if (y < 293 + 125 && x > 8607 && x < 9131)
	{
		kolisioa = 1;
	}
	else if (y > 268 && y < 293 + 125 && x > 9131 && x - 35 < 9255)
	{
		kolisioa = 1;
	}
	else if (y > 154 && y < 179 + 125 && x > 9325 && x - 35 < 9426)
	{
		kolisioa = 1;
	}
	else if (y > 154 && x > 9426 && x < 9500)
	{
		kolisioa = 1;
	}
	else if ((y > 449) && ((x > 9500) && (x < 10000)))
	{
		kolisioa = 1;
	}
	else if ((y >= 426) && (y <= 449) && (x - 35 > 9750) && (x < 9822))
	{
		irten = 3;
	}
}

void SaltoaMarraztuR(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	if (ikusi == 1)
	{
		EtsaiaMugitu();
	}

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT: //bi funtzio hauen eta goiko bi funtzio haien diferentzia nagusia, hanken mugimendua da. Funtzio hauetan, hankak ez dira mugitzen ezkerrera edo eskumara egitean.
					PertsonaiaEzkerreraSaltoR(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoR(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuR(pertsonaiaX, Y);

	if (kolisioa == 0) {
		pertsonaiaY = Y;
		if (norabidea == 2)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuR(t0, y0, vy);
	}
	else if (kolisioaEtsaiakPertsonaia == 1) {

		if (norabidea == 2)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		kolisioaEtsaiakPertsonaia = 0;

		ikusi = 0;

		t0 = SDL_GetTicks64();

		SaltoaMarraztuR(t0, pertsonaiaY, 40);
	}
	else
	{
		jausi();
	}
}

void PertsonaiaEzkerreraSaltoR(int Y)
{
	kolisioaDetektatuR(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			mapaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaX -= 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		norabidea = 2;
	}
}

void PertsonaiaEskumaraSaltoR(int Y)
{
	kolisioaDetektatuR(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 10;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			mapaX += 10;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaX += 15;
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		norabidea = 1;
	}
}

void jausi()
{
	kolisioaDetektatuR(pertsonaiaX, pertsonaiaY + 7);

	while (kolisioa == 0) //kolisioa ez dagoen bitartean, lurra ez dagoen bitartean, pertsonaia jausi egingo da.
	{
		pertsonaiaY += 7;
		if (norabidea == 1)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuR(pertsonaiaX, pertsonaiaY + 7);
	}

	if (pertsonaiaY >= 550) // hemen sartzen bada, pertsonaia hutsunera jausi egin da.
	{
		irten = 2;
	}

	pertsonaiaY += 7;
	kolisioaDetektatuR(pertsonaiaX, pertsonaiaY);
	while (kolisioa == 1)
	{
		kolisioaDetektatuR(pertsonaiaX, pertsonaiaY - 1);
		pertsonaiaY -= 1;
	}

	if (norabidea == 1)
	{
		pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
}

void EtsaiakMarraztuR()
{
	//3 etsai mota daude.
	if (EtsaiaAukeratu == 1)
	{
		kargatuEtsaiak(".\\Argazkiak\\Redes-bmp\\Etsaiak\\PC.bmp");
	}
	else if (EtsaiaAukeratu == 2)
	{
		kargatuEtsaiak(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Router.bmp");
	}
	else if (EtsaiaAukeratu == 3)
	{
		kargatuEtsaiak(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Switch.bmp");
	}

	SDL_Rect dstrect;

	dstrect.x = EtsaiakX - mapaX;
	dstrect.y = EtsaiakY;
	dstrect.w = 150;
	dstrect.h = 102;

	SDL_RenderCopy(Render, EtsaiakT, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void EtsaiaAukeratuRandom()
{
	//1 eta 3 zenbakien artean, bat hausaz eman.
	EtsaiaAukeratu = rand() % 3 + 1;
}

void EtsaiaMugitu()
{
	KolisioaEtsaiakPertsonaia();

	if (kolisioaEtsaiakPertsonaia == 0) //kolisioa ez dagoen bitartean, etsaia ezkerrerantz mugituko da.
	{
		EtsaiakX -= 15;
		if (EtsaiaAukeratu == 1)
		{
			EtsaiakMarraztuR(".\\Argazkiak\\Redes-bmp\\Etsaiak\\PC.bmp");
		}
		else if (EtsaiaAukeratu == 2)
		{
			EtsaiakMarraztuR(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Router.bmp");
		}
		else if (EtsaiaAukeratu == 3)
		{
			EtsaiakMarraztuR(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Switch.bmp");
		}	

		if (norabidea == 2)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
	}
	else if (kolisioaEtsaiakPertsonaia == 1) //pertsonaiak, etsaiaren gainean egin du salto. Pertsonaiak bizitza bat irabaziko du eta etsaia desagertu egingo da.
	{
		if (bizitzaR < 10)
		{
			bizitzaR++;
		}
		posEtsaia++;
		ikusi = 0;
		posiziozAldatuEtsaia();
		/*if (norabidea == 2)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}*/
	}
	else if (kolisioaEtsaiakPertsonaia == 2) //etsaiak, pertsonaia jan egindu. Pertsonaiak bizitza bat galduko du eta etsaia desagertu egingo da
	{
		if (bizitzaR > 0)
		{
			bizitzaR--;
		}
		posEtsaia++;
		ikusi = 0;
		posiziozAldatuEtsaia();
		if (norabidea == 2)
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuR(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
	}
}

void KolisioaEtsaiakPertsonaia()
{
	//Etsaien eta pertsonaien artean 2 kolisioa posible:
	// 1- Pertsonaiak, etsaiaren gainean salto egin.
	kolisioaEtsaiakPertsonaia = 0;

	if ((pertsonaiaX + mapaX >= EtsaiakX) && (pertsonaiaX + mapaX <= EtsaiakX + 150) && (pertsonaiaY + 125 >= EtsaiakY))
	{
		if (pertsonaiaY != 449 - 125)
		{
			kolisioaEtsaiakPertsonaia = 1;
		}
		else //2- Etsaiak, pertsonaia jan du
		{
			kolisioaEtsaiakPertsonaia = 2;
		}
	}
}

void posiziozAldatuEtsaia()
{
	//etsaiak mapako posizio finkoetan marraztuko dira.
	if (posEtsaia == 0)
	{
		EtsaiakX = 1120;
	}
	else if (posEtsaia == 1)
	{
		EtsaiakX = 3333;
	}
	else if (posEtsaia == 2)
	{
		EtsaiakX = 4000;
	}
	else if (posEtsaia == 3)
	{
		EtsaiakX = 5800;
	}
	else if (posEtsaia == 4)
	{
		EtsaiakX = 7600;
	}
	else if (posEtsaia == 5)
	{
		EtsaiakX = 8750;
	}
	else
	{
		EtsaiakX = 0;
	}
}