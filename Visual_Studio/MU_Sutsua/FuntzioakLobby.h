#ifndef FUNTZIOAKLOBBY_H
#define FUNTZIOAKLOBBY_H

void mainLobby();

void mapaKargatu();
void MapaMarraztu();
void pertsonaiaMarraztu(char helbidea[]);
void O2MezuaMarraztu(char helbidea[]);

void kutxaMarraztu();
void apunteakMarraztu(int X, int Y);
void errepikatzaileaMarraztu();
void azalpenaApunteenaMarraztu();

void Ezkerrera();
void Eskumara();
void Gora();
void Behera();

void kolisioaDetektatu(int x, int y);
void kolisioaDetektatu0(int x, int y);
void kolisioaDetektatu1(int x, int y);
void kolisioaDetektatu2(int x, int y);
void kolisioaDetektatu3(int x, int y);
void kolisioaDetektatu4(int x, int y);
void kolisioaDetektatu5(int x, int y);

void mapaAldatu();
void mapaZatikaMarraztu();

void igogailua();

void lorategia();
void ureztagailuaMarraztu();
void kafeMakina();
void marraztuKafeMakina();

void apunteakLortu(int x, int y);

void hasierakoAnimazioa();
void bossaMarraztuL();
void notakMarraztu();

void marraztuKafeMakina();
void kafeMakina();

void oxigenorikGabeGelditu();

#endif
