#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakLobby.h"
#include "FuntzioakOinarriBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakOinarri.h"

int norabidea;

void mainOinarri()
{
	//aldagai globalen deklarazioa
	pertsonaiaX = 120;
	pertsonaiaY = 449 - 125;
	mapaX = 0;

	pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten)) //bukle nagusia. bukle hau jokoa martxan dagoen bitartean ez da geldituko. Mugimendu sistema
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuO(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					PertsonaiaEzkerreraO();
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraO();
					break;
				}
			}
		}
	}

	if (irten == 3)
	{
		irten = 0;
		SDL_Delay(500);
		mainOinarriBoss();
	}
	else if (irten == 2)
	{
		irten = 0;
		if (bizitzaO > 0)
		{
			bizitzaO--;
			SDL_Delay(500);
			mainOinarri();
		}
		else
		{
			SDL_Delay(500);
			mainLobby();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaO = 0;
		mainLobby();
	}
	else
	{
		bizitzaO = 0;
	}
}

void pertsonaiaMarraztuO(char helbidea[])
{
	mapaMarraztuO(); //mapa marraztu

	kargatuPertsonaia(helbidea); //pertsonaia marraztu

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaO);  //pertsonaiaren bizitza marraztu
	//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);

	//EtsaienMugimendua();
}

void mapaMarraztuO()
{
	//mapa bi marrazkitan banatua dago
	if (mapaX + pertsonaiaX <= 5000)
	{
		kargatuIrudia(".\\Argazkiak\\Oinarri-bmp\\OinarriNibela1.bmp");//maparen lehen zatia marraztu

		SDL_Rect mapa;

		mapa.x = mapaX;
		mapa.y = 0;
		mapa.w = 1200;
		mapa.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	}
	else
	{
		kargatuIrudia(".\\Argazkiak\\Oinarri-bmp\\OinarriNibela2.bmp"); //maparen bigarren zatia marraztu

		SDL_Rect mapa2;

		mapa2.x = mapaX - 4000;
		mapa2.y = 0;
		mapa2.w = 1200;
		mapa2.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa2, NULL);
	}

	/*SDL_Rect dstRect;

	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = 1200;
	dstRect.h = 600;*/

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);
}

void PertsonaiaEzkerreraO()
{
	kolisioaDetektatuO(pertsonaiaX - 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik


	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))//pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 2;
		jausiO();
	}
}
void PertsonaiaEskumaraO()
{
	kolisioaDetektatuO(pertsonaiaX + 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))//pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			pertsonaiaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 1;
	}
	jausiO();
}

void PertsonaiaEzkerreraSaltoO(int Y)
{
	kolisioaDetektatuO(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			mapaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaX -= 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		norabidea = 2;
	}
}
void PertsonaiaEskumaraSaltoO(int Y)
{
	kolisioaDetektatuO(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 10;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			mapaX += 10;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaX += 15;
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		norabidea = 1;
	}
}

void kolisioaDetektatuO(int x, int y)
{
	//mapan dauden kolisio guztiak kalkulatzen ditu. Horrela mapari sentzua emanez.
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 10000))
	{
		kolisioa = 1;
	}
	else if (y - 125 > 600)
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x < 900))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 900) && (x < 975))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x >= 975) && (x < 1124))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 1124) && (x < 1199))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 1199) && (x < 2475))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x >= 2475) && (x - 35 < 2550))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 35 > 2550) && (x - 35 < 2699))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x + 35 > 2699) && (x - 35 < 2774))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x + 35 > 2774) && (x - 35 < 3674))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x + 35 > 3750) && (x - 35 < 3824))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 35 > 3900) && (x - 35 < 3974))
	{
		kolisioa = 1;
	}
	else if ((y > 225) && (x + 35 > 4050) && (x - 35 < 4124))
	{
		kolisioa = 1;
	}
	else if ((y > 225) && (x + 35 > 4200) && (x - 35 < 4274))
	{
		kolisioa = 1;
	}
	else if ((y > 225) && (x + 35 > 4350) && (x - 35 < 4424))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x + 35 > 4500) && (x - 35 < 4574))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x + 35 > 4650) && (x - 35 < 4724))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x + 35 > 4800) && (x - 35 < 4874))
	{
		kolisioa = 1;
	}
	else if ((y > 525) && (x + 35 > 4950) && (x - 35 < 5024))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 5102) && (x < 6525))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 6525) && (x < 6600))
	{
		kolisioa = 1;
	}
	else if ((y > 300) && (x > 6600) && (x < 7874))
	{
		kolisioa = 1;
	}
	else if ((y > 375) && (x > 7874) && (x < 7949))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 7949) && (x < 8324))
	{
		kolisioa = 1;
	}
	else if ((y > 599) && (x + 35 > 8324) && (x - 35 < 9075))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 9075) && x < 10000)
	{
		kolisioa = 1;
	}
	else if ((y < 74 + 125) && (x > 900) && (x < 575))
	{
		kolisioa = 1;
	}
	else if ((y < 113 + 125) && (x > 975) && (x < 1200))
	{
		kolisioa = 1;
	}
	else if ((y < 149 + 125) && (x > 1200) && (x < 1275))
	{
		kolisioa = 1;
	}
	else if ((y < 224 + 125) && (x > 1275) && (x < 1574))
	{
		kolisioa = 1;
	}
	else if ((y < 149 + 125) && (x > 1574) && (x < 2025))
	{
		kolisioa = 1;
	}
	else if ((y < 224 + 125) && (x > 2025) && (x < 2399))
	{
		kolisioa = 1;
	}
	else if ((y < 149 + 125) && (x > 2399) && (x < 2474))
	{
		kolisioa = 1;
	}
	else if ((y < 113 + 125) && (x > 2474) && (x < 2699))
	{
		kolisioa = 1;
	}
	else if ((y < 74 + 125) && (x > 2699) && (x < 2774))
	{
		kolisioa = 1;
	}
	else if ((y < 74 + 125) && (x > 4875) && (x < 5100))
	{
		kolisioa = 1;
	}
	else if ((y < 149 + 125) && (x > 5100) && (x < 5699))
	{
		kolisioa = 1;
	}
	else if ((y < 74 + 125) && (x > 5699) && (x < 5774))
	{
		kolisioa = 1;
	}
	else if ((y > 278) && (x > 9691) && (x < 9780))
	{
		irten = 3;
	}
}

void SaltoaMarraztuO(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT://bi funtzio hauen eta goiko bi funtzio haien diferentzia nagusia, hanken mugimendua da. Funtzio hauetan, hankak ez dira mugitzen ezkerrera edo eskumara egitean.
					PertsonaiaEzkerreraSaltoO(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoO(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuO(pertsonaiaX, Y);

	if (kolisioa == 0)
	{
		pertsonaiaY = Y;
		if (norabidea == 2)
		{
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuO(t0, y0, vy);
	}
	else
	{
		jausiO();
	}
}
void jausiO()
{
	kolisioaDetektatuO(pertsonaiaX, pertsonaiaY + 7);

	while (kolisioa == 0)//kolisioa ez dagoen bitartean, lurra ez dagoen bitartean, pertsonaia jausi egingo da.
	{
		pertsonaiaY += 7;
		if (norabidea == 1)
		{
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuO(pertsonaiaX, pertsonaiaY + 7);
	}

	if (pertsonaiaY >= 550)// hemen sartzen bada, pertsonaia hutsunera jausi egin da.
	{
		irten = 2;
	}

	pertsonaiaY += 7;
	kolisioaDetektatuO(pertsonaiaX, pertsonaiaY);
	while (kolisioa == 1)
	{
		kolisioaDetektatuO(pertsonaiaX, pertsonaiaY - 1);
		pertsonaiaY -= 1;
	}

	if (norabidea == 1)
	{
		pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuO(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
}