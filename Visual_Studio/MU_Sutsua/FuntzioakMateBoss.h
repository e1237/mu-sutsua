#ifndef FUNTZIOAKMATEBOSS_H
#define FUNTZIOAKMATEBOSS_H

void mainMateBoss();

void pertsonaiaMarraztuMB(char helbidea[]);
void bossaMarraztuMB(char helbidea[]);
void TxanponaLortuMB();
void TxanponaMugituMB();
void mapaMarraztuMB();

void bokadilloaMarraztuMB(char helbidea[]);

void EzkerreraMB();
void EskumaraMB();

void SaltoaMarraztuMB(float t0, int y0, int vy);
void JausiMB();

void ariketarenEmaitzaOndoTxarto();
int zenbakiaEragiketaSortu(int nonaino);

void bossMugimenduaMB();
void kolisioaPertsonaiaBossMB();

void kolisioaDetektatuMB(int x, int y);

void hilMB(int nor);

#endif
