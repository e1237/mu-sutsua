#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakLobby.h"
#include "FuntzioakRedes.h"
#include "FuntzioakOinarriBoss.h"
#include "FuntzioakRedesBoss.h"
#include "FuntzioakFisikaBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakOinarri.h"
#include "FuntzioakFisika.h"
#include "FuntzioakMate.h"
#include "FuntzioakProgram.h"

int Norabidea = 1;
int Mapa;

void mainLobby()
{
	mapaX = 0;
	pertsonaiaX = 90;
	pertsonaiaY = 300;
	Mapa = 0;
	bossY = 130;

	pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	if (hasiera == 1)
	{
		SDL_Delay(1000);
		hasierakoAnimazioa();
	}

	if (ureztatu == 1)
	{
		SDL_Delay(500);
		O2MezuaMarraztu(".\\Argazkiak\\Lobby-bmp\\OxigenorikEz.bmp");
		SDL_Delay(1500);
		pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");
		O2MezuaMarraztu(".\\Argazkiak\\Lobby-bmp\\UreztatuAzkar.bmp");
		SDL_Delay(1500);
		T0 = SDL_GetTicks64();
	}

	SDL_Event e;

	pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	while ((!kendu) && (!irten))
	{
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT) {
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
				case SDLK_ESCAPE:
					kendu = 1;
				case SDLK_UP:
					Gora();
					break;
				case SDLK_DOWN:
					Behera();
					break;
				case SDLK_LEFT:
					Ezkerrera();
					break;
				case SDLK_RIGHT:
					Eskumara();
					break;
				}
			}
		}

		if (ureztatu == 1)
		{
			T = SDL_GetTicks64() - T0;
			if (T >= 15000)
			{
				oxigenorikGabeGelditu();
			}
		}
	}

	if (irten == 2)
	{
		irten = 0;
		SDL_Delay(500);
		if (kolisioa == 11)
		{
			bizitzaR = 5 + apunteKop;
			mainRedes();
		}
		else if (kolisioa == 13)
		{
			bizitzaO = 5 + apunteKop;
			mainOinarri();
		}
		else if (kolisioa == 21)
		{
			bizitzaM = 5 + apunteKop;
			mainMate();
		}
		else if (kolisioa == 23)
		{
			bizitzaF = 5 + apunteKop;
			mainFisika();
		}
		else if (kolisioa == 31)
		{
			bizitzaP = apunteKop;
			mainProgram();
		}
	}
}

void MapaMarraztu()
{
	mapaKargatu();

	SDL_Rect srcrect;

	srcrect.x = mapaX;
	srcrect.y = 0;
	srcrect.w = 1200;
	srcrect.h = 600;

	//SDL_BlitSurface(irudia, &srcrect, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &srcrect, NULL);
}

void mapaKargatu()
{
	if (Mapa == 0)
	{
		if (ureztatu == 1)
		{
			kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\HasierakoMapa2.bmp");
		}
		else
		{
			kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\HasierakoMapa.bmp");
		}
	}
	else if (Mapa == 1)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\1_solairua.bmp");
	}
	else if (Mapa == 2)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\2_Solairua.bmp");
	}
	else if (Mapa == 3)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\Azken_solairua2.bmp");
	}
	else if (Mapa == 4)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\Reku.bmp");
	}
	else if (Mapa == 5)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\ApunteaGela.bmp");
	}
	else if (Mapa == 6)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\LorategiaGaizki.bmp");
	}
	else if (Mapa == 7)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\LorategiaOndo.bmp");
	}
	else if (Mapa == 8)
	{
		kargatuIrudia(".\\Argazkiak\\Lobby-bmp\\kafe-makina.bmp");
	}
}

void pertsonaiaMarraztu(char helbidea[])
{
	MapaMarraztu();

	if (Mapa == 5)
	{
		if (kutxaIrekita == 0)
		{
			azalpenaApunteenaMarraztu();
		}
		kutxaMarraztu();
		errepikatzaileaMarraztu();
	}

	kargatuPertsonaia(helbidea);

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void O2MezuaMarraztu(char helbidea[])
{
	kargatuEtsaiak(helbidea);

	SDL_Rect dstrect;

	dstrect.x = 255;
	dstrect.y = 215;
	dstrect.w = 690;
	dstrect.h = 170;

	SDL_RenderCopy(Render, EtsaiakT, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void apunteakMarraztu(int X, int Y)
{
	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\Apunteak.bmp");

	SDL_Rect dstrect;

	if (kolisioa != 10)
	{
		dstrect.x = X - mapaX;
	}
	else
	{
		dstrect.x = X;
	}
	dstrect.y = Y;
	dstrect.w = 91;
	dstrect.h = 98;

	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
}

void kutxaMarraztu()
{
	if (kutxaIrekita == 0)
	{
		kargatuKutxa(".\\Argazkiak\\Lobby-bmp\\Kofrea-itxita.bmp");
	}
	else
	{
		kargatuKutxa(".\\Argazkiak\\Lobby-bmp\\Kofrea-irekita.bmp");
	}

	SDL_Rect dstrect;

	dstrect.x = 541 - mapaX;
	dstrect.y = 206;
	dstrect.w = 218;
	dstrect.h = 188;

	SDL_RenderCopy(Render, Kutxa, NULL, &dstrect);
}

void errepikatzaileaMarraztu()
{
	kargatuErrepikatzailea(".\\Argazkiak\\Lobby-bmp\\Errepikatzailea.bmp");

	SDL_Rect dstrect;

	dstrect.x = 800 - mapaX;
	dstrect.y = 170;
	dstrect.w = 56;
	dstrect.h = 224;

	SDL_RenderCopy(Render, Errepikatzailea, NULL, &dstrect);
}

void azalpenaApunteenaMarraztu()
{
	kargatuErasoa(".\\Argazkiak\\Lobby-bmp\\BokadilloaRepe.bmp");

	SDL_Rect dstrect;

	dstrect.x = 550 - mapaX;
	dstrect.y = 20;
	dstrect.w = 250;
	dstrect.h = 150;

	SDL_RenderCopy(Render, ErasoaT, NULL, &dstrect);
}

void Ezkerrera()
{
	kolisioaDetektatu(pertsonaiaX - 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}

	Norabidea = 2;

	if (kolisioa == 20)
	{
		apunteakLortu(1318, 418);
	}
}

void Eskumara()
{
	kolisioaDetektatu(pertsonaiaX + 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
	}

	Norabidea = 1;
}

void Gora()
{
	kolisioaDetektatu(pertsonaiaX, pertsonaiaY - 30);

	if (kolisioa == 0)
	{
		if (Norabidea == 1)
		{
			pertsonaiaY -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaY -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaY -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaY -= 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}
	else if (kolisioa == 20)
	{
		apunteakLortu(604, 180);
	}
	else if (kolisioa >= 2)
	{
		mapaAldatu();
	}
}

void Behera()
{
	kolisioaDetektatu(pertsonaiaX, pertsonaiaY + 30);

	if (kolisioa == 0)
	{
		if (Norabidea == 1)
		{
			pertsonaiaY += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaY += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaY += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaY += 15;
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(100);
		}
	}
	else if (((kolisioa >= 2) && (kolisioa <= 7)) || (kolisioa == 9) || (kolisioa >= 11))
	{
		mapaAldatu();
	}
}

void kolisioaDetektatu(int x, int y)
{
	kolisioa = 0;

	y += 100;
	x += mapaX + 35;

	if (Mapa == 0)
	{
		kolisioaDetektatu0(x, y);
	}
	else if (Mapa == 1)
	{
		kolisioaDetektatu1(x, y);
	}
	else if (Mapa == 2)
	{
		kolisioaDetektatu2(x, y);
	}
	else if (Mapa == 3)
	{
		kolisioaDetektatu3(x, y);
	}
	else if (Mapa == 4)
	{
		kolisioaDetektatu4(x, y);
	}
	else if (Mapa == 5)
	{
		kolisioaDetektatu5(x, y);
	}

	//Maparen arabera bertako kolisioetara bideratzen du funtzioa

	/*kolisioa = 0 => ez dago ezer
	* kolisioa = 1 => pareta
	* kolisioa = 2 => Hasierako Mapara
	* kolisioa = 3 => 1_solairua
	* kolisioa = 4 => 2_solairua
	* kolisioa = 5 => Azken_solairua
	* kolisioa = 6 => Rekutik 1_solairura
	* kolisioa = 7 => Apunteetaik 2_solairura
	* kolisioa = 8 => Landareak
	* kolisioa = 9 => Igogailu
	* kolisioa = 10 => Kafe makina
	* kolisioa = 11 => 1. gela, Redes
	* kolisioa = 12 => 2. gela, Reku fisika
	* kolisioa = 13 => 3. gela, Oinarri
	* kolisioa = 20 => Apunteak
	* kolisioa = 21 => 4. gela, Mate
	* kolisioa = 22 => 5. gela, Apunteen gela
	* kolisioa = 23 => 6. gela, Fisika
	* kolisioa = 31 => Azken gela, Program
	* kolisioa = 92 => Igogailutik 1. solairura
	* kolisioa = 93 => Igogailutik 2. solairura
	* kolisioa = 94 => Igogailutik 3. solairura
	* kolisioa = 95 => Igogailutik Azken solairura
	*/
}

void kolisioaDetektatu0(int x, int y)
{
	//lobby-ko kolisioak

	if ((x <= 0) || (x >= 1500) || (y >= 600))
	{
		kolisioa = 1;
	}
	else if (y <= 271)
	{
		if (((x >= 890) && (x <= 1249)) || ((x >= 1360) && (x <= 1400)))
		{
			kolisioa = 1;
		}
		else if ((x >= 122) && (x <= 362))
		{
			kolisioa = 9;
			//igogailua
		}
		else if ((x > 362) && (x <= 780))
		{
			kolisioa = 8;
			//landareak
		}
		else if (y <= 79)
		{
			if ((x >= 1249) && (x <= 1360))
			{
				kolisioa = 10;
				//kafe-makina
			}
			else
			{
				kolisioa = 3;
				//1. pisua
			}
		}
	}
}

void kolisioaDetektatu1(int x, int y)
{
	//1. pisuko kolisioak

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if ((y <= 180) && (x >= 941) && (x <= 1125))
	{
		kolisioa = 1;
	}
	else if (y <= 160)
	{
		if ((x <= 180) || ((x >= 260) && (x <= 713)) || ((x >= 793) && (x <= 1239)) || (x >= 1319))
		{
			kolisioa = 1;
		}
		else if (y <= 120)
		{
			if ((x >= 180) && (x <= 260))
			{
				kolisioa = 11;
				//Redes
			}
			else if ((x >= 713) && (x <= 793))
			{
				if (nibela >= 1)
				{
					kolisioa = 12;
					//Reku
				}
				else
				{
					kolisioa = 1;
				}
			}
			else if ((x >= 1239) && (x <= 1319))
			{
				if (nibela >= 1)
				{
					kolisioa = 13;
					//Oinarri
				}
				else
				{
					kolisioa = 1;
				}

			}
			else
			{
				kolisioa = 1;
			}
		}
	}
	else if (y >= 380)
	{
		if (((x >= 362) && (x <= 636)) || ((x >= 760) && (x <= 765)) || ((x >= 890) && (x <= 1246)) || ((x >= 1360) && (x <= 1380)))
		{
			kolisioa = 1;
		}
		else if ((x >= 122) && (x <= 350))
		{
			kolisioa = 9;
			//igogailua
		}
		else if (y >= 550)
		{
			if (((x >= 636) && (x <= 760)) || ((x >= 1246) && (x <= 1372)))
			{
				if (nibela >= 2)
				{
					kolisioa = 4;
					//2. pisua
				}
				else
				{
					kolisioa = 1;
				}
			}
			else
			{
				kolisioa = 2;
				//Hasierkao pisua
			}
		}
	}
}

void kolisioaDetektatu2(int x, int y)
{
	//2. pisuko kolisioak

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if ((y <= 180) && (x >= 340) && (x <= 590))
	{
		kolisioa = 1;
	}
	else if (y <= 160)
	{
		if ((x <= 180) || ((x >= 260) && (x <= 713)) || ((x >= 793) && (x <= 1239)) || (x >= 1319))
		{
			kolisioa = 1;
		}
		else if (y <= 120)
		{
			if ((x >= 180) && (x <= 260))
			{
				if (nibela >= 2)
				{
					kolisioa = 21;
					//Mate
				}
				else
				{
					kolisioa = 1;
				}
			}
			else if ((x >= 713) && (x <= 793))
			{
				if (nibela >= 3)
				{
					kolisioa = 22;
					//Apunteen gela
				}
				else
				{
					kolisioa = 1;
				}
			}
			else if ((x >= 1239) && (x <= 1319))
			{
				if (nibela >= 3)
				{
					kolisioa = 23;
					//Fisika
				}
				else
				{
					kolisioa = 1;
				}
			}
			else
			{
				kolisioa = 1;
			}
		}
	}
	else if (y >= 380)
	{
		if (((x >= 350) && (x <= 636)) || ((x >= 760) && (x <= 765)) || ((x >= 890) && (x <= 1246)) || ((x >= 1360) && (x <= 1380)))
		{
			kolisioa = 1;
		}
		else if ((x >= 122) && (x <= 350))
		{
			kolisioa = 9;
			//igogailua
		}
		else if (y >= 550)
		{
			if (((x >= 636) && (x <= 760)) || ((x >= 1246) && (x <= 1372)))
			{
				kolisioa = 3;
				//1. pisua
			}
			else
			{
				if (nibela >= 4)
				{
					kolisioa = 5;
					//Azken pisua
				}
				else
				{
					kolisioa = 1;
				}
			}
		}
	}
}

void kolisioaDetektatu3(int x, int y)
{
	//Azken solairuko kolisioak

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if (y <= 230)
	{
		if ((x <= 710) || (x >= 790))
		{
			kolisioa = 1;
		}
		else if (y <= 120)
		{
			if ((x >= 710) && (x <= 790))
			{
				kolisioa = 31;
				//Program
			}
			else
			{
				kolisioa = 1;
			}
		}
	}
	else if (y >= 380)
	{
		if (((x >= 350) && (x <= 762)) || ((x >= 890) && (x <= 1372)))
		{
			kolisioa = 1;
		}
		else if ((x >= 122) && (x <= 350))
		{
			kolisioa = 9;
			//igogailua
		}
		else if (y >= 550)
		{
			kolisioa = 4;
			//2. pisua
		}
	}
}

void kolisioaDetektatu4(int x, int y)
{
	//Rekuko kolisioak

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if (y <= 290)
	{
		if ((x >= 126) && (x <= 252))
		{
			kolisioa = 6;
			//1_Solairua
		}
		else
		{
			kolisioa = 1;
		}
	}
	else if ((y >= 380) && (x <= 1409))
	{
		if (((y >= 506) && (y <= 573) && (x <= 1409)) && (rekuEginda == 0))
		{
			kolisioa = 20;
		}
		else
		{
			kolisioa = 1;
		}
	}
	else if (x > 1409)
	{
		if (y >= 600)
		{
			kolisioa = 1;
		}
	}
}

void kolisioaDetektatu5(int x, int y)
{
	//Apunte gelako kolisioak

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if ((y <= 394) && (x >= 540) && (x <= 860))
	{
		if ((x >= 540) && (x <= 760))
		{
			if (kutxaIrekita == 0)
			{
				kolisioa = 20;
				//Kutxa
			}
			else
			{
				kolisioa = 1;
			}
		}
		else if ((x >= 760) && (x <= 860))
		{
			kolisioa = 1;
		}
	}
	else if (y <= 290)
	{
		if ((x >= 126) && (x <= 252))
		{
			kolisioa = 7;
			//2_Solairua
		}
		else
		{
			kolisioa = 1;
		}
	}
	else if (y >= 600)
	{
		kolisioa = 1;
	}
}

void mapaAldatu()
{
	int borratzekoPos;

	if (kolisioa == 2)
	{
		pertsonaiaY = 271 - 140;
		Mapa = 0;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");

		//Lobby-ra
	}
	else if (kolisioa == 92)
	{
		pertsonaiaY = 180;
		Mapa = 0;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//Igogailutik lobby-ra
	}
	else if (kolisioa == 3)
	{
		pertsonaiaY = 350;
		Mapa = 1;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//1. solairura
	}
	else if (kolisioa == 93)
	{
		pertsonaiaY = 260;
		Mapa = 1;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//Igogailutik 1. solairura
	}
	else if (kolisioa == 4)
	{
		pertsonaiaY = 350;
		Mapa = 2;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//2. solairura
	}
	else if (kolisioa == 94)
	{
		pertsonaiaY = 260;
		Mapa = 2;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//igogailutik 2. solairura
	}
	else if (kolisioa == 5)
	{
		pertsonaiaY = 350;
		Mapa = 3;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//Azken solairura
	}
	else if (kolisioa == 95)
	{
		pertsonaiaY = 260;
		Mapa = 3;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//igogailutik azken solairura
	}
	else if (kolisioa == 6)
	{
		mapaX = 120;
		pertsonaiaY = 50;
		pertsonaiaX = 610;
		Mapa = 1;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//Rekutik 1. pisura
	}
	else if (kolisioa == 7)
	{
		mapaX = 120;
		pertsonaiaY = 50;
		pertsonaiaX = 610;
		Mapa = 2;
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
		//Apunte gelatik 2. solairura
	}
	else if (kolisioa == 8)
	{
		if (ureztatu == 1)
		{
			Mapa = 6;
			//Lorategia ureztatu gabe
		}
		else
		{
			Mapa = 7;
			//Lorategia ureztatuta
		}
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_MU_Sutsua.bmp");
	}
	else if (kolisioa == 9)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Lobby-bmp\\Igogailua.bmp");
		//igogailua
	}
	else if (kolisioa == 10)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Lobby-bmp\\kafe-makina.bmp");
		// kafe-makina
	}
	else if (kolisioa == 11)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Redes.bmp");
		//Redes
	}
	else if (kolisioa == 12)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Reku.bmp");
		Mapa = 4;
		pertsonaiaX = 160;
		pertsonaiaY = 200;
		mapaX = 0;
		//Reku
	}
	else if (kolisioa == 13)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Oinarri.bmp");
		//Oinarri
	}
	else if (kolisioa == 21)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Mate.bmp");
		//Mate
	}
	else if (kolisioa == 22)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Apunteak.bmp");
		Mapa = 5;
		pertsonaiaX = 160;
		pertsonaiaY = 200;
		mapaX = 0;
		//Apunte gela
	}
	else if (kolisioa == 23)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Fisika.bmp");
		//Fisika
	}
	else if (kolisioa == 31)
	{
		kargatuKargaPantaila(".\\Argazkiak\\Pantailak-bmp\\Pantaila_Program.bmp");
		//Program
	}

	mapaZatikaMarraztu();

	if ((kolisioa >= 11) && (kolisioa <= 31) && (kolisioa != 12) && (kolisioa != 22))
	{
		irten = 2;
	}

	if ((kolisioa < 8) || (kolisioa > 31) || (kolisioa == 12) || (kolisioa == 22))
	{
		pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else if (kolisioa == 9)
	{
		igogailua();
		if (kolisioa != 9)
		{
			mapaAldatu();
		}
	}
	else if (kolisioa == 10)
	{
		kafeMakina();
	
		if (kolisioa != 10)
		{
			mapaAldatu();
		}
	}
	else if (kolisioa == 8)
	{
		lorategia();
	}
}

void igogailua()
{
	int x = 0, y = 0;
	int irten = 0;

	SDL_Event event;

	while (irten == 0)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				SDL_GetMouseState(&x, &y);
				irten = 1;
			}
		}
	}
	/*
	Uint32 botoiak = 0;

	while ((botoiak & SDL_BUTTON_LMASK) == 0) {
		SDL_PumpEvents();
		botoiak = SDL_GetMouseState(&x, &y);

	}*/
	
	if ((x >= 152) && (x <= 231))
	{
		if ((y >= 175) && (y <= 254))
		{
			kolisioa = 92;
		}
		else if ((y >= 350) && (y <= 429))
		{
			kolisioa = 93;
		}
	}
	else if ((x >= 622) && (x <= 701))
	{
		if ((y >= 175) && (y <= 254))
		{
			if (nibela >= 2)
			{
				kolisioa = 94;
			}
		}
		else if ((y >= 350) && (y <= 429))
		{
			if (nibela >= 4)
			{
				kolisioa = 95;
			}
		}
	}
}

void kafeMakina()
{
	int x = 0, y = 0;

	Uint32 botoiak = 0;

	if (txanpona != 0)
	{
		while ((botoiak & SDL_BUTTON_LMASK) == 0) {
			SDL_PumpEvents();
			botoiak = SDL_GetMouseState(&x, &y);
		}

		if ((x >= 988) && (x <= 1114) && (y >= 290) && (y <= 372));
		{
			while (txanpona != 0)
			{
				apunteakLortu(450, 250);
				marraztuKafeMakina();
				txanpona--;
				SDL_Delay(1000);
			}

		}
	}
}

void marraztuKafeMakina() {
	SDL_Rect srcrect;

	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = 1200;
	srcrect.h = 600;

	SDL_RenderCopy(Render, kargaPantaila, &srcrect, NULL);
	SDL_RenderPresent(Render);
}

void lorategia()
{
	int borratzekoPos;

	MapaMarraztu();
	SDL_RenderPresent(Render);

	if (ureztatu == 1)
	{
		O2MezuaMarraztu(".\\Argazkiak\\Lobby-bmp\\Enter.bmp");

		SDL_Event Enter;

		while (ureztatu == 1)
		{
			if (SDL_PollEvent(&Enter) != 0) {
				if (Enter.type == SDL_KEYDOWN) {
					if (Enter.key.keysym.sym == SDLK_RETURN)
					{
						MapaMarraztu();
						SDL_RenderPresent(Render);

						ureztatu = 0;
					}
				}
			}
		}

		ureztagailuaMarraztu();

		SDL_Delay(1000);

		kargatuKargaPantaila(".\\Argazkiak\\Lobby-bmp\\LorategiaOndo.bmp");

		mapaZatikaMarraztu();

		SDL_Delay(500);

		ureztatu = 0;
	}

	Mapa = 0;
}

void mapaZatikaMarraztu()
{
	int borratzekoPos;

	for (borratzekoPos = 0; borratzekoPos <= 1200; borratzekoPos += 50)
	{
		SDL_Rect Nondik;

		Nondik.x = 0;
		Nondik.y = 0;
		Nondik.w = borratzekoPos;
		Nondik.h = 600;

		SDL_RenderCopy(Render, kargaPantaila, &Nondik, &Nondik);
		SDL_RenderPresent(Render);

		SDL_Delay(50);
	}
}

void ureztagailuaMarraztu()
{
	kargatuErasoa(".\\Argazkiak\\Lobby-bmp\\Ureztagailua.bmp");

	SDL_Rect Non;

	Non.x = 600;
	Non.y = 200;
	Non.w = 168;
	Non.h = 152;

	SDL_RenderCopy(Render, ErasoaT, NULL, &Non);
	SDL_RenderPresent(Render);
}

void apunteakLortu(int x, int y)
{
	int i;

	if (Mapa == 5)
	{
		kutxaIrekita = 1;
	}
	else if (Mapa == 4)
	{
		rekuEginda = 1;
	}

	for (i = 1; i <= 50; i++)
	{
		if (kolisioa != 10)
		{
			if (Norabidea == 1)
			{
				pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			}
			else
			{
				pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			}
		}
		else
		{
			marraztuKafeMakina();
		}

		apunteakMarraztu(x, y - i);
		SDL_RenderPresent(Render);
		SDL_Delay(20);
	}

	if (kolisioa != 10)
	{
		if (Norabidea == 1)
		{
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
	}

	apunteKop++;
}

void hasierakoAnimazioa()
{
	int i;

	for (i = 0; i <= 120; i += 5)
	{
		bossY += 5;
		pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");
		bossaMarraztuL();
		SDL_RenderPresent(Render);
	}

	SDL_Delay(500);
	pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEskOuch.bmp");
	bossaMarraztuL();
	SDL_RenderPresent(Render);
	notakMarraztu();
	SDL_Delay(3000);

	for (i = 0; i <= 120; i += 5)
	{
		pertsonaiaMarraztu(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEskOuch.bmp");
		bossaMarraztuL();
		SDL_RenderPresent(Render);
		bossY -= 5;
	}

	hasiera = 0;
}

void bossaMarraztuL()
{
	kargatuBossa(".\\Argazkiak\\Program-bmp\\Etsaiak\\Enaitz.bmp");

	SDL_Rect dstrect;

	dstrect.x = 150;
	dstrect.y = bossY;
	dstrect.w = 194;
	dstrect.h = 179;

	SDL_RenderCopy(Render, BossT, NULL, &dstrect);

}

void notakMarraztu()
{
	kargatuKargaPantaila(".\\Argazkiak\\Lobby-bmp\\Notak.bmp");

	SDL_Rect dstrect;

	dstrect.x = 500;
	dstrect.y = 0;
	dstrect.w = 800;
	dstrect.h = 600;

	SDL_RenderCopy(Render, kargaPantaila, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void oxigenorikGabeGelditu()
{
	O2MezuaMarraztu(".\\Argazkiak\\Lobby-bmp\\NotakJaitsi.bmp");
	if (nibela >= 0)
	{
		bizitzaR--;
	}
	if (nibela >= 1)
	{
		bizitzaO--;
	}
	if (nibela >= 2)
	{
		bizitzaM--;
	}
	if (nibela >= 3)
	{
		bizitzaF--;
	}
	if (nibela >= 4)
	{
		bizitzaP--;
	}

	SDL_Delay(1000);

	ureztatu = 0;
}