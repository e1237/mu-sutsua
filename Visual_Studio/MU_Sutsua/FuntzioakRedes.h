#ifndef FUNTZIOAKREDES_H
#define FUNTZIOAKREDES_H

void mainRedes();

void pertsonaiaMarraztuR(char helbidea[]);
void mapaMarraztuR();

void PertsonaiaEzkerrera();
void PertsonaiaEskumara();

void PertsonaiaEzkerreraSaltoR(int Y);
void PertsonaiaEskumaraSaltoR(int Y);

void kolisioaDetektatuR(int x, int y);

void SaltoaMarraztuR(float t0, int y0, int vy);
void jausi();

void EtsaiakMarraztuR();
void EtsaiaAukeratuRandom();
//void kokatuEtsaia();
void posiziozAldatuEtsaia();
void EtsaiaMugitu();
void KolisioaEtsaiakPertsonaia();
//void EtsaiaIkustenAhalDa();


#endif // ! FUNTZIOAKREDES_H
/*
void EtsaiakMarraztuR(char helbidea);
void EtsaienMugimendua();
int KolisioaEtsaiakPertsonaia(int etsaiaX, int etsaiaY);
*/