#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakRedesBoss.h"
#include "FuntzioakRedes.h"
#include "FuntzioakSDL.h"

int NorabideaRB = 1;
int norabideaUhinak;
int mugimenduKopRB;
int bossImg;
int itxaron;
int ikusi;
int bizitzaRB;

void mainRedesBoss()
{
	//aldagaien deklarazioa
	pertsonaiaX = 240;
	pertsonaiaY = 520 - 125;
	mapaX = 0;
	
	ikusi = 0;
	mugimenduKopRB = 0;
	norabideaUhinak = 0;
	bossImg = 1;

	bizitzaBoss = 3;
	bizitzaRB = bizitzaR;

	erasoaX = 1100 - 108;
	erasoaY = 370;

	bossX = 1100;
	bossY = 520 - 174;

	pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten))
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				//pertsonaiaren mugimendua
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuRB(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					EzkerreraRB();
					break;
				case SDLK_RIGHT:
					EskumaraRB();
					break;
				}
				if (bizitzaBoss > 0) //Boss-a bizirik dagoen bitartea, mugitu eta atakeak egingo ditu
				{
					if ((mugimenduKopRB == 10) && (norabideaUhinak == 0))
					{
						norabideaUhinak = 1;
						bossImg = 2;
					}
					else
					{
						mugimenduKopRB++;
					}
				}
			}
		}

		if ((norabideaUhinak != 0) && (bizitzaBoss > 0))
		{
			erasoaMugituRB();
		}
	}
	if (irten == 2)
	{
		irten = 0;
		if (bizitzaBoss == 0)
		{
			SDL_Delay(500);
			if (nibela == 0)
			{
				nibela++;
			}
			ureztatu = 1;
			mainLobby();
		}
		else
		{
			SDL_Delay(500);
			XabiMugituRB();
			SDL_Delay(500);
			mainRedesBoss();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaR = 0;
		mainLobby();
	}
	else
	{
		bizitzaR = 0;
	}
}

void pertsonaiaMarraztuRB(char helbidea[])
{
	if(bizitzaRB > 0) 
	{
		mapaMarraztuRB();
		
		kargatuPertsonaia(helbidea);
		
		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

	    //SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	   //SDL_UpdateWindowSurface(screen);

	   bizitzaMarraztu(bizitzaRB);
	   bihotzaMarraztu(bizitzaBoss);

	   bossaMarraztuRB(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Karlos.bmp");

	   SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	   SDL_RenderPresent(Render);
    }
	else
	{
		hilRB(1);
		irten = 2;
	}
}

void bossaMarraztuRB()
{
	if (bizitzaBoss > 0)
	{
		if( bossImg == 1)
		{ 
			kargatuBossa(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Karlos.bmp");
		}
		else
		{
			kargatuBossa(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Karlos_Golpe.bmp");
		}
	

		SDL_Rect dstrect;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 204;
		dstrect.h = 174;

		//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
	else
	{
		if (bossX != 1250)
		{
			hilRB(2);
			SDL_Delay(500);
			TxanponaMugituRB();
		}

		kargatuBossa(".\\Argazkiak\\Lobby-bmp\\Teleport.bmp");

		SDL_Rect dstrect;

		bossY = 520 - 173;
		bossX = 1250;


		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 90;
		dstrect.h = 173;

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
}

void XabiMarraztuRB()
{
	mapaMarraztuRB();
	bossaMarraztuRB();
	kargatuEtsaiak(".\\Argazkiak\\Redes-bmp\\Etsaiak\\Xabi.bmp");

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;		
	dstrect.y = XabiY;
	dstrect.w = 174;
	dstrect.h = 177;

	SDL_RenderCopy(Render, EtsaiakT, NULL, &dstrect);
	SDL_RenderPresent(Render);

}

void XabiMugituRB()
{
	XabiY = 300;
	int i;

	for (i = 1; i <= 100;i++)
	{
		XabiY--;
		XabiMarraztuRB();
	}
}

void TxanponaLortuRB()
{
	mapaMarraztuRB();
	
	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaRB);
	bihotzaMarraztu(bizitzaBoss);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	SDL_RenderPresent(Render);

	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\txanponak.bmp");

	dstrect.x = 565;
	dstrect.y = TxanponaY;
	dstrect.w = 75;
	dstrect.h = 75;


	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void TxanponaMugituRB()
{
	TxanponaY = 300;
	int i;

	for (i = 1; i <= 100; i++)
	{
		TxanponaY--;
		TxanponaLortuRB();
	}
	
	txanpona++;
	
}

void erasoaMarraztuRB(char helbidea[])
{
	if (bizitzaBoss > 0)
	{
		kargatuErasoa(helbidea);

		SDL_Rect dstrect;

		dstrect.x = erasoaX - mapaX;
		dstrect.y = erasoaY;
		dstrect.w = 108;
		dstrect.h = 99;

		//SDL_BlitSurface(Erasoa, NULL, Superficie, &dstrect);

		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, ErasoaT, NULL, &dstrect);
		SDL_RenderPresent(Render);
	}
}

void erasoaMugituRB()
{
	kolisioaDetektatuRB(pertsonaiaX, pertsonaiaY);
	
	if (norabideaUhinak == 1)
	{
		erasoaX -= 10;
		erasoaMarraztuRB(".\\Argazkiak\\Redes-bmp\\Ondak_Karlos.bmp");
	}
	if ((erasoaX <= 0) || (erasoaX + 100 >= bossX) || (kolisioa == 3))
	{
		bossImg = 1;
		if (kolisioa == 3)
		{
			bizitzaRB--;
		}
		else if ((erasoaX + 100 >= bossX))
		{
			bizitzaBoss--;
			bizitzaRB++;
		}
		
		mugimenduKopRB = 0;
		norabideaUhinak = 0;
		erasoaX = 1100 - 108;
	}

	if (NorabideaRB == 1)
	{
		pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
}

void mapaMarraztuRB()
{
	kargatuIrudia(".\\Argazkiak\\Redes-bmp\\Estrella_de_la_muerte_barrutik.bmp");

	SDL_Rect mapa;

	mapa.x = mapaX;
	mapa.y = 0;
	mapa.w = 1200;
	mapa.h = 600;

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
}

void EzkerreraRB()
{
	int i;

	kolisioaDetektatuRB(pertsonaiaX - 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		/*for (i = 1; i <= 10; i++)
		{
			if ((mapaX + pertsonaiaX - 3 >= 600) && (mapaX + pertsonaiaX - 3 <= 900))
			{
				mapaX -= 3;
				//SDL_Delay(50);
			}
			else
			{
				pertsonaiaX -= 3;
				//SDL_Delay(50);
			}

			if (i <= 5)
			{
				pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			}
			else
			{
				pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			}
		}
		*/
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}

	NorabideaRB = 1;
}

void EskumaraRB()
{
	int i;

	kolisioaDetektatuRB(pertsonaiaX + 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		/*for (i = 1; i <= 10; i++)
		{
			if ((mapaX + pertsonaiaX + 3 >= 600) && (mapaX + pertsonaiaX + 3 <= 900))
			{
				mapaX += 3;
				//SDL_Delay(50);
			}
			else
			{
				pertsonaiaX += 3;
				//SDL_Delay(50);
			}

			if (i <= 7)
			{
				pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			}
			else
			{
				pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			}
		}
		*/
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
	}

	NorabideaRB = 2;
}

void PertsonaiaEzkerreraSaltoRB(int Y)
{
	kolisioaDetektatuRB(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
		}

		NorabideaRB = 1;
	}
}

void PertsonaiaEskumaraSaltoRB(int Y)
{
	kolisioaDetektatuRB(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
		}
		NorabideaRB = 2;
	}
}

void kolisioaDetektatuRB(int x, int y)
{
	kolisioa;

	y += 125;
	x += mapaX;

	if ((x <= 83) || (x + 69 >= 1416))
	{
		kolisioa = 1;
	}
	else if (y > 520)
	{
		kolisioa = 1;
	}
	else if ((x > bossX) && (y > bossY + 25) && (x < bossX + 204) && (bizitzaBoss > 0))
	{
		kolisioa = 2;
	}
	else if ((y >= erasoaY) && (x >= erasoaX) && (x <= erasoaX + 108) && (norabideaUhinak == 1))
	{
		kolisioa = 3;
	}
	else if ((bizitzaBoss == 0) && (x - 35 >= bossX) && (x <= bossX + 90) && (y > bossY))
	{
		irten = 2;
	}
	else
	{
		kolisioa = 0;
	}
}

void SaltoaMarraztuRB(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 150;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	if (norabideaUhinak != 0)
	{
		erasoaMugituRB();
	}

	SDL_Event event;

	if (vy == 40)
	{
		if ((mapaX + pertsonaiaX - 5 >= 600) && (mapaX + pertsonaiaX - 5 <= 900))
		{
			mapaX -= 5;
		}
		else
		{
			pertsonaiaX -= 5;
		}
	}
	else
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					PertsonaiaEzkerreraSaltoRB(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoRB(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuRB(pertsonaiaX, Y);

	if ((kolisioa == 0) || (kolisioa == 3)){
		pertsonaiaY = Y;
		if (NorabideaRB == 1)
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuRB(t0, y0, vy);
	}
	else if (kolisioa == 2) {

		if (NorabideaRB == 1)
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		t0 = SDL_GetTicks64();


		SaltoaMarraztuRB(t0, pertsonaiaY, 40);
	}
	else
	{
		if (vy == 40)
		{
			bizitzaBoss--;
			bizitzaRB++;
		}
		jausiRB();
		if (NorabideaRB == 1)
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
	}
}

void jausiRB()
{
	kolisioaDetektatuRB(pertsonaiaX, pertsonaiaY + 5);

	while (kolisioa == 0)
	{
		pertsonaiaY += 5;
		if (NorabideaRB == 1)
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuRB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuRB(pertsonaiaX, pertsonaiaY + 5);
	}
}

void hilRB(int nor)
{
	while ((pertsonaiaY < 610) && (bossY < 610))
	{
		mapaMarraztuRB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaRB);

		if (nor == 1)
		{
			bossaMarraztuRB();
			pertsonaiaY += 7;
		}
		else
		{
			bossY += 7;

			SDL_Rect dstrect;

			dstrect.x = bossX - mapaX;
			dstrect.y = bossY;
			dstrect.w = 204;
			dstrect.h = 174;

			//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
			//SDL_UpdateWindowSurface(screen);

			SDL_RenderCopy(Render, BossT, NULL, &dstrect);
		}

		SDL_RenderPresent(Render);
	}
}