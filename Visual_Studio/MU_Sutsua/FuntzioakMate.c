#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakLobby.h"
#include "FuntzioakMateBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakMate.h"

int norabidea;

void mainMate()
{
	//aldagai globalen deklarazioa
	pertsonaiaX = 120;
	pertsonaiaY = 449 - 125;
	mapaX = 0;

	pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten)) //bukle nagusia. bukle hau jokoa martxan dagoen bitartean ez da geldituko. Mugimendu sistema
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuM(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					PertsonaiaEzkerreraM();
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraM();
					break;
				}
			}
		}
	}

	if (irten == 3)
	{
		irten = 0;
		SDL_Delay(500);
		mainMateBoss();
	}
	else if (irten == 2)
	{
		irten = 0;
		if (bizitzaM > 0)
		{
			bizitzaM--;
			SDL_Delay(500);
			mainMate();
		}
		else
		{
			SDL_Delay(500);
			mainLobby();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaM = 0;
		mainLobby();
	}
	else
	{
		bizitzaM = 0;
	}
}

void pertsonaiaMarraztuM(char helbidea[])
{
	mapaMarraztuM(); //mapa marraztu

	kargatuPertsonaia(helbidea); //pertsonaia marraztu

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaM);  //pertsonaiaren bizitza marraztu
	//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);
	SDL_RenderPresent(Render);

	//EtsaienMugimendua();
}

void mapaMarraztuM()
{
	//mapa bi marrazkitan banatua dago
	if (mapaX + pertsonaiaX <= 5000)
	{
		kargatuIrudia(".\\Argazkiak\\Mate-bmp\\MateNibela1.bmp");//maparen lehen zatia marraztu

		SDL_Rect mapa;

		mapa.x = mapaX;
		mapa.y = 0;
		mapa.w = 1200;
		mapa.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	}
	else
	{
		kargatuIrudia(".\\Argazkiak\\Mate-bmp\\MateNibela2.bmp"); //maparen bigarren zatia marraztu

		SDL_Rect mapa2;

		mapa2.x = mapaX - 4000;
		mapa2.y = 0;
		mapa2.w = 1200;
		mapa2.h = 600;

		SDL_RenderCopy(Render, irudiaT, &mapa2, NULL);
	}
}

void PertsonaiaEzkerreraM()
{
	kolisioaDetektatuM(pertsonaiaX - 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik


	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");//pertsonaia maparen erdida heltzen denean, mapa mugituko da
			SDL_Delay(10);
			mapaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(10);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 2;
		jausiM();
	}
}

void PertsonaiaEskumaraM()
{
	kolisioaDetektatuM(pertsonaiaX + 30, pertsonaiaY);//pertsonaiaren inguruan kolisioak dauden edo ez kalkulaten du, 
	//erantzuna 0 baldin bada, aurrera jarraituko du, ez dagoelako kolisiorik

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))//pertsonaia maparen erdida heltzen denean, mapa mugituko da
		{
			mapaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			mapaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		else//hasiera batean pertsonaia da mugitzen dena
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(10);
			pertsonaiaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(10);
		}
		norabidea = 1;
	}
	jausiM();
}

void PertsonaiaEzkerreraSaltoM(int Y)
{
	kolisioaDetektatuM(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 9400))
		{
			mapaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			mapaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			pertsonaiaX -= 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		norabidea = 2;
	}
}

void PertsonaiaEskumaraSaltoM(int Y)
{
	kolisioaDetektatuM(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 9400))
		{
			mapaX += 10;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			mapaX += 10;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			pertsonaiaX += 15;
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		norabidea = 1;
	}
}

void kolisioaDetektatuM(int x, int y)
{
	//mapan dauden kolisio guztiak kalkulatzen ditu. Horrela mapari sentzua emanez.
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 10000))
	{
		kolisioa = 1;
	}
	else if (y - 125 > 600)
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x < 1000))
	{
		kolisioa = 1;
	}
	else if ((y > 328) && (x + 35 > 1038) && (x - 35 < 1109))
	{
		kolisioa = 1;
	}
	else if ((y > 184) && (x + 35 > 1182) && (x - 35 < 1253))
	{
		kolisioa = 1;
	}
	else if ((y > 266) && (x + 35 > 1400) && (x - 35 < 1471))
	{
		kolisioa = 1;
	}
	else if ((y > 175) && (x + 35 > 1602) && (x - 35 < 1673))
	{
		kolisioa = 1;
	}
	else if ((y > 314) && (x + 35 > 1775) && (x - 35 < 1846))
	{
		kolisioa = 1;
	}
	else if ((y > 451) && (x > 1942) && (x < 2417))
	{
		kolisioa = 1;
	}
	else if ((y > 384) && (x > 2417) && (x < 2659))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 2659) && (x < 2770))
	{
		kolisioa = 1;
	}
	else if ((y > 304) && (x > 2770) && (x < 2849))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 2849) && (x < 2890))
	{
		kolisioa = 1;
	}
	else if ((y > 223) && (x > 2980) && (x < 3210))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 3186) && (x < 4037))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 4131) && (x < 4221))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 4327) && (x < 4513))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 4603) && (x < 4789))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 4899) && (x < 5613))
	{
		kolisioa = 1;
	}
	else if ((y > 323) && (x > 5613) && (x < 5737))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 5737) && (x < 6041))
	{
		kolisioa = 1;
	}
	else if ((y > 359) && (x > 6102) && (x < 6173))
	{
		kolisioa = 1;
	}
	else if ((y > 256) && (x > 6242) && (x < 6390))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 6409) && (x < 6685))
	{
		kolisioa = 1;
	}
	else if ((y > 369) && (x > 6735) && (x < 6818))
	{
		kolisioa = 1;
	}
	else if ((y > 264) && (x > 6916) && (x < 7039))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 7085) && (x < 7956))
	{
		kolisioa = 1;
	}
	else if ((y > 420) && (x > 7956) && (x < 7988))
	{
		kolisioa = 1;
	}
	else if ((y > 401) && (x > 7988) && (x < 8030))
	{
		kolisioa = 1;
	}
	else if ((y > 351) && (x > 8030) && (x < 9071))
	{
		kolisioa = 1;
	}
	else if ((y > 401) && (x > 9071) && (x < 9100))
	{
		kolisioa = 1;
	}
	else if ((y > 420) && (x > 9100) && (x < 9132))
	{
		kolisioa = 1;
	}
	else if ((y > 450) && (x > 9132) && (x < 10000))
	{
		kolisioa = 1;
	}
	else if ((y > 202) && (x > 9620) && (x < 9910))
	{
		irten = 3;
	}
}

void SaltoaMarraztuM(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	SDL_Event event;

	if (vy != 40)
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT://bi funtzio hauen eta goiko bi funtzio haien diferentzia nagusia, hanken mugimendua da. Funtzio hauetan, hankak ez dira mugitzen ezkerrera edo eskumara egitean.
					PertsonaiaEzkerreraSaltoM(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoM(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuM(pertsonaiaX, Y);

	if (kolisioa == 0)
	{
		pertsonaiaY = Y;
		if (norabidea == 2)
		{
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuM(t0, y0, vy);
	}
	else
	{
		jausiM();
	}
}

void jausiM()
{
	kolisioaDetektatuM(pertsonaiaX, pertsonaiaY + 7);

	while (kolisioa == 0)//kolisioa ez dagoen bitartean, lurra ez dagoen bitartean, pertsonaia jausi egingo da.
	{
		pertsonaiaY += 7;
		if (norabidea == 1)
		{
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuM(pertsonaiaX, pertsonaiaY + 7);
	}

	if (pertsonaiaY >= 550)// hemen sartzen bada, pertsonaia hutsunera jausi egin da.
	{
		irten = 2;
	}

	pertsonaiaY += 7;
	kolisioaDetektatuM(pertsonaiaX, pertsonaiaY);
	while (kolisioa == 1)
	{
		kolisioaDetektatuM(pertsonaiaX, pertsonaiaY - 1);
		pertsonaiaY -= 1;
	}

	if (norabidea == 1)
	{
		pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
	}
	else
	{
		pertsonaiaMarraztuM(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
	}
}