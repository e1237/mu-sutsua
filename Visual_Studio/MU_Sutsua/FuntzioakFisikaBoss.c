#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakFisikaBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakLobby.h"


int NorabideaFB = 1;
int BossPostura = 0;

void mainFisikaBoss()
{
	//aldagaien deklarazioa
	pertsonaiaX = 180;
	pertsonaiaY = 511 - 125;
	mapaX = 0;

	bizitzaBoss = 3;
	bizitzaF = 5;
	
	erasoaX = 500;
	erasoaY = -160;

	bossX = 1100;
	bossY = 511 - 207;

	pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten))
	{
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN)
			{ //pertsonaiaren mugimendu sistema
				switch (e.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuFB(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					EzkerreraFB();
					break;
				case SDLK_RIGHT:
					EskumaraFB();
					break;
				}

			}
		}
		if (erasoaY < 600) //erasoak lurra ikutu arte, beherantz joango da
		{
			erasoaMugituFB();
		}
		else
		{
			erasoaY = -160;
			erasoaX = randomizatuX();
		}	
	}

	if (irten == 2)
	{
		irten = 0;
		if (bizitzaBoss == 0)
		{
			SDL_Delay(500);
			if (nibela == 3)
			{
				nibela++;
			}
			ureztatu = 1;
			mainLobby();
		}
		else
		{
			if (bizitzaF == 0)
			{
				hilF(1);
				ureztatu = 1;
				mainLobby();
			}
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaF = 0;
		mainLobby();
	}
	else
	{
		bizitzaF = 0;
	}
}

void mapaMarraztuFB()
{
	kargatuIrudia(".\\Argazkiak\\Fisika-bmp\\Olimpo-aurrebista.bmp");

	SDL_Rect mapa;

	mapa.x = mapaX;
	mapa.y = 0;
	mapa.w = 1200;
	mapa.h = 600;

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
}

void pertsonaiaMarraztuFB(char helbidea[])
{
	if (bizitzaF > 0)
	{
		mapaMarraztuFB();

		kargatuPertsonaia(helbidea);

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		//SDL_BlitSurface(pertsonaia, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		bossaMarraztuFB();

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaF);
		bihotzaMarraztu(bizitzaBoss);

		SDL_RenderPresent(Render);

		/*if (mugimenduKop == 10)
		{
			norabideaPunch = 1;
			mugimenduKop++;
		}
		else
		{
			mugimenduKop++;
		}*/
	}
}

void EzkerreraFB()
{
	kolisioaDetektatuFB(pertsonaiaX - 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))//pertsonaia pantailaren erdira heltzen denean, mapa mugitzen hasi
		{
			mapaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
		else//hasiera batean, pertsonaia mugituko da. Pantailaren erdira heldu arte
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}
	NorabideaFB = 1;
	
}

void EskumaraFB()
{
	kolisioaDetektatuFB(pertsonaiaX + 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX +30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
	}

	NorabideaFB = 2;
}

void kolisioaDetektatuFB(int x, int y)
{	
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x - 35 <= 47) || (x + 35 >= 1453))
	{	kolisioa = 1;
	}
	else if (y > 511)
	{
		kolisioa = 1;
	}
	else if ((bizitzaBoss > 0) && (x >= bossX) && (x <= bossX + 180) && (y > bossY))
	{
		kolisioa = 2;
	}
	else if ((bizitzaBoss == 0) && (x - 35 > bossX) && (y > bossY) && (x < bossX + 90))
	{
		irten = 2;
	}

	erasoarekinKolisioaFB(x, y);
}

void erasoarekinKolisioaFB(int x, int y)
{	
	if ((erasoaX < pertsonaiaX + mapaX + 35) && (erasoaX > pertsonaiaX + mapaX - 35) && (erasoaY > pertsonaiaY) && (erasoaY < pertsonaiaY + 125))
	{
		if (bizitzaF > 1) //erasoaren eta pertsonairen artean kolisioa baldin badago, pertsonaiak bizitza puntu bat galduko du
		{
			bizitzaF--;
			erasoaY = -160;
			erasoaX = randomizatuX();
		}
		else //pertsonairen bizitza 0 baldin bada, hil egingo da eta lobby-ra joango da
		{
			bizitzaF--;
			irten = 2;
		}
	}
}

void SaltoaMarraztuFB(int t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 150;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	if (erasoaY < 600) //erasoa lurrera iritsi arte, beherantz joango da
	{
		erasoaMugituFB();
	}
	else
	{
		erasoaY = -160;
		erasoaX = randomizatuX();
	}

	SDL_Event event;

	if (vy == 40)
	{
		if ((mapaX + pertsonaiaX - 5 >= 600) && (mapaX + pertsonaiaX - 5 < 900))
		{
			mapaX -= 5;
		}
		else
		{
			pertsonaiaX -= 5;
		}
	}
	else
	{
		if (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					PertsonaiaEzkerreraSaltoFB(Y);
					break;
				case SDLK_RIGHT:
					PertsonaiaEskumaraSaltoFB(Y);
					break;
				}
			}
		}
	}

	kolisioaDetektatuFB(pertsonaiaX, Y);

	if (kolisioa == 0) {
		pertsonaiaY = Y;
		if (NorabideaFB == 1)
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		SaltoaMarraztuFB(t0, y0, vy);
	}
	else if (kolisioa == 2) {

		if (NorabideaFB == 1)
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}

		t0 = SDL_GetTicks64();

		SaltoaMarraztuFB(t0, pertsonaiaY, 40);
	}

	else
	{
		if (vy == 40)
		{
			bizitzaBoss--;
			if (bizitzaF < 10)
			{
				bizitzaF++;
			}
		}
		jausiFB();
	}
}

void PertsonaiaEzkerreraSaltoFB(int Y)
{
	kolisioaDetektatuFB(pertsonaiaX - 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 <= 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
		}

		NorabideaFB = 1;
	}
}

void PertsonaiaEskumaraSaltoFB(int Y)
{
	kolisioaDetektatuFB(pertsonaiaX + 30, Y);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))
		{
			mapaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
		}
		NorabideaFB = 2;
	}
}

void jausiFB()
{
	kolisioaDetektatuFB(pertsonaiaX, pertsonaiaY + 5);

	while (kolisioa == 0)
	{
		pertsonaiaY += 5;
		if (NorabideaFB == 1)
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuFB(pertsonaiaX, pertsonaiaY + 5);
	}
}

void bossaMarraztuFB()
{
	if (bizitzaBoss > 0)
	{
		if (BossPostura == 0)
		{
			kargatuBossa(".\\Argazkiak\\Fisika-bmp\\Etsaiak\\Ion.bmp");
		}
		else
		{
			kargatuBossa(".\\Argazkiak\\Fisika-bmp\\Etsaiak\\IonHaserre.bmp");
		}

		SDL_Rect dstrect;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 121;
		dstrect.h = 207;

		//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
	else
	{
		if (bossX != 1105 )
		{
			hilF(2);
			SDL_Delay(500);
			TxanponaMugituFB();
		}

		kargatuBossa(".\\Argazkiak\\Lobby-bmp\\Teleport.bmp");

		SDL_Rect dstrect;

		bossY = 511 - 173;
		bossX = 1105;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 90;
		dstrect.h = 173;

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
}

void hilF(int nor)
{
	while ((pertsonaiaY < 600) && (bossY < 600))
	{
		mapaMarraztuFB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaF);

		if (nor == 1)
		{
			bossaMarraztuFB();
			pertsonaiaY += 7;
		}
		else
		{
			bossY += 7;

			SDL_Rect dstrect;

			dstrect.x = bossX - mapaX;
			dstrect.y = bossY;
			dstrect.w = 121;
			dstrect.h = 207;

			//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
			//SDL_UpdateWindowSurface(screen);

			SDL_RenderCopy(Render, BossT, NULL, &dstrect);
		}

		SDL_RenderPresent(Render);
	}
}
void TxanponaLortuFB()
{
	mapaMarraztuFB();

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaF);
	bihotzaMarraztu(bizitzaBoss);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	SDL_RenderPresent(Render);

	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\txanponak.bmp");

	dstrect.x = 565;
	dstrect.y = TxanponaY;
	dstrect.w = 75;
	dstrect.h = 75;


	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
	SDL_RenderPresent(Render);
}
void TxanponaMugituFB()
{
	TxanponaY = 300;
	int i;

	for (i = 1; i <= 100; i++) //txanpona agertzerakoan, gorantz joango da
	{
		TxanponaY--;
		TxanponaLortuFB();
	}

	txanpona++;

}

void erasoaMarraztuFB(char helbidea[])
{
	if (bizitzaBoss > 0) //erasoa marrazteko, boss-a bizirik egon beharko da
	{
		kargatuErasoa(helbidea);

		SDL_Rect dstrect;

		dstrect.x = erasoaX - mapaX;
		dstrect.y = erasoaY;
		dstrect.w = 70;
		dstrect.h = 160;

		SDL_RenderCopy(Render, ErasoaT, NULL, &dstrect);
		SDL_RenderPresent(Render);
	}
}

void erasoaMugituFB()
{
	kolisioaDetektatuFB(pertsonaiaX, pertsonaiaY);

	if (((bizitzaBoss == 1) || (bizitzaBoss == 2)) && (bizitzaF > 0))
	{
		BossPostura = 1; //boss-ak bi itxura ezberdin dauzka. BossPostura = 1 --> Haserre

		if (erasoaY == -160)
		{
			T0 = SDL_GetTicks64();
		}

		T = SDL_GetTicks64();

		erasoaY = T - T0;

		erasoaMarraztuFB(".\\Argazkiak\\PNG\\Fisika\\TximistaDefinitivo.bmp");

		if (NorabideaFB == 1)
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuFB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
	}
}

int randomizatuX()
{
	return (rand() % 600) + 500; //erasoak, X ardatzean, 500etik 1100ra hausaz
}