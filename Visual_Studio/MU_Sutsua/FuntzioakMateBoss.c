#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <SDL.h>

#include "FuntzioakMateBoss.h"
#include "FuntzioakSDL.h"
#include "FuntzioakMate.h"

int norabideaPertsonaiaMB = 1, helduDa, testua, emaitzaOndoTxarto, kolisioaPertsonaiaBoss, bizitzaBossTotal;

void mainMateBoss()
{
	//aldagaien deklarazioa
	pertsonaiaX = 150;
	pertsonaiaY = 350 - 125;
	mapaX = 0;

	bossX = 1200;
	bossY = 350 - 228;

	helduDa = 0;
	testua = 0;
	emaitzaOndoTxarto = 0;

	bizitzaBoss = 3;
	bizitzaBossTotal = 3;

	pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk.bmp");

	SDL_Event e;

	while ((!kendu) && (!irten))
	{
		if (helduDa > 0 && helduDa < 7) //pertsonaia geldiune puntura heltzean, beraien arteko elkarrizketa hasi egiten da
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEskLo.bmp");
			SDL_Delay(3000);
			helduDa++;
		}
		else if (helduDa == 7) //beraien arteko elkarrizketa bukatzean, konsolan eragiketak atera.
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEskLo.bmp");
			ariketarenEmaitzaOndoTxarto();
			bossMugimenduaMB();
			kolisioaPertsonaiaBossMB();
			if (kolisioaPertsonaiaBoss == 1) //etsaiak, pertsonaiaren posizioara heltzen baldin bada, pertsonaia hil egingo da
			{
				bossX = 1200;
				bizitzaM = 0;
			}
			if (emaitzaOndoTxarto == 1)
			{
				bizitzaBoss -= 1;
			}
			else
			{
				bizitzaBoss = 3;
				bizitzaM -= 1;

				//etsai nagusiari bizitza bat kentzeko, pertsonaiak 3 eragiketa ondo egin beharko ditu. SEGIDAN.
			}
			if (bizitzaBoss == 0)
			{
				bizitzaBossTotal -= 1;
				if (bizitzaM < 10)
				{
					bizitzaM++;
				}
				bizitzaBoss = 3;
				if (bizitzaBossTotal == 2)
				{
					printf("\nHurrengokoak zailagoak izango dira. Hori ez da problemia!\n");
				}
				else if (bizitzaBossTotal == 1)
				{
					printf("\nEz duzu matematika gaindituko!! EZINEZKOA DA!!.\n");
				}
			}
			if (bizitzaBossTotal == 0) //etsai nagusia hlitzerakoan, pertsonaia berriz ere mugitu ahalko da
			{
				printf("\nAzterketa errexegia izan da. Hurrengo urtean ikusiko gara!\n");
				helduDa = 0;
				hilMB(2);
			}
		}
		if (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				kendu = 1;
			}
			else if (e.type == SDL_KEYDOWN && helduDa == 0) //helduDa != 0 denean, ezin izango da mugitu.
			{												//elkarrizketa osoan eta eragiketak egiten ari den momementuan, pertsonaia ezin izango da mugitu
				switch (e.key.keysym.sym)
				{//pertsonairen mugimendu sistema
				case SDLK_ESCAPE:
					irten = 1;
				case SDLK_UP:
					SaltoaMarraztuMB(SDL_GetTicks64(), pertsonaiaY, 65);
					break;
				case SDLK_LEFT:
					EzkerreraMB();
					norabideaPertsonaiaMB = 2;
					break;
				case SDLK_RIGHT:
					EskumaraMB();
					norabideaPertsonaiaMB = 1;
					kolisioaPertsonaiaBossMB();
					break;
				}
			}
		}
	}

	if (irten == 2)
	{
		irten = 0;
		if (bizitzaBossTotal == 0)
		{
			SDL_Delay(500);
			if (nibela == 2)
			{
				nibela++;
			}
			ureztatu = 1;
			mainLobby();
		}
		else
		{
			hilMB(1);
			SDL_Delay(500);
			ureztatu = 1;
			mainLobby();
		}
	}
	else if (irten == 1)
	{
		irten = 0;
		bizitzaM = 0;
		mainLobby();
	}
	else
	{
		bizitzaM = 0;
	}
}

void pertsonaiaMarraztuMB(char helbidea[])
{
	mapaMarraztuMB();

	kargatuPertsonaia(helbidea);

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bossaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Etsaiak\\German.bmp");

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	bizitzaMarraztu(bizitzaM);
	bihotzaMarraztu(bizitzaBossTotal);

	if (helduDa != 0 && helduDa < 7) //etsaiaren eta pertsonaiaren arteko elkarrizketa
	{
		if (testua == 0)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa1.bmp");
		}
		else if (testua == 1)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa2.bmp");
		}
		else if (testua == 2)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa3.bmp");
		}
		else if (testua == 3)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa4.bmp");
		}
		else if (testua == 4)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa5.bmp");
		}
		else if (testua == 5)
		{
			bokadilloaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Bokadilloa6.bmp");
		}
		testua++;
	}

	SDL_RenderPresent(Render);
}

void bossaMarraztuMB(char helbidea[])
{
	if (bizitzaBossTotal > 0)
	{
		kargatuBossa(helbidea);

		SDL_Rect dstrect;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 155;
		dstrect.h = 243;

		//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
		//SDL_UpdateWindowSurface(screen);

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
	else
	{
		if (bossX != 1205)
		{
			hilMB(2);
			SDL_Delay(500);
			TxanponaMugituMB();
		}

		kargatuBossa(".\\Argazkiak\\Lobby-bmp\\Teleport.bmp"); //boss-a hiltzean, ate beltza agertu

		SDL_Rect dstrect;

		bossY = 350 - 173;
		bossX = 1205;

		dstrect.x = bossX - mapaX;
		dstrect.y = bossY;
		dstrect.w = 90;
		dstrect.h = 173;

		SDL_RenderCopy(Render, BossT, NULL, &dstrect);
	}
}

void mapaMarraztuMB()
{
	kargatuIrudia(".\\Argazkiak\\Mate-bmp\\ikasgela.bmp");

	SDL_Rect mapa;

	mapa.x = mapaX;
	mapa.y = 0;
	mapa.w = 1200;
	mapa.h = 600;

	//SDL_BlitSurface(irudia, &mapa, Superficie, NULL);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, irudiaT, &mapa, NULL);
	SDL_RenderPresent(Render);
}

void TxanponaLortuMB()
{
	mapaMarraztuMB();

	SDL_Rect dstrect;

	dstrect.x = pertsonaiaX;
	dstrect.y = pertsonaiaY;
	dstrect.w = 69;
	dstrect.h = 125;

	bizitzaMarraztu(bizitzaM);
	bihotzaMarraztu(bizitzaBoss);

	SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

	SDL_RenderPresent(Render);

	kargatuObjektua(".\\Argazkiak\\Lobby-bmp\\txanponak.bmp");

	dstrect.x = 565;
	dstrect.y = TxanponaY;
	dstrect.w = 75;
	dstrect.h = 75;


	SDL_RenderCopy(Render, Objektua, NULL, &dstrect);
	SDL_RenderPresent(Render);
}

void TxanponaMugituMB()
{
	TxanponaY = 300;
	int i;

	for (i = 1; i <= 100; i++) //txanpona agertzerakoan, gorantz egingo du
	{
		TxanponaY--;
		TxanponaLortuMB();
	}

	txanpona++;
}

void bokadilloaMarraztuMB(char helbidea[])
{
	kargatuEtsaiak(helbidea);

	SDL_Rect dstrect;

	dstrect.x = bossX - 400;
	dstrect.y = bossY - 100;
	dstrect.w = 250;
	dstrect.h = 150;
	if (testua == 5) //pertsonaiak hitz egiterakoan, bokadilloa beste leku baten barraztu behar da
	{
		dstrect.x = bossX - 500;
		dstrect.y = bossY - 50;
	}

	//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
	//SDL_UpdateWindowSurface(screen);

	SDL_RenderCopy(Render, EtsaiakT, NULL, &dstrect);
}

void EskumaraMB()
{
	kolisioaDetektatuMB(pertsonaiaX + 30, pertsonaiaY);

	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX + 30 > 600) && (mapaX + pertsonaiaX + 30 <= 900))//pertsonaia pantailaren erdira heltzean, mapa mugitzen hasi
		{
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else//hasiera batean, pertsonaia mugituko da. Pantailaren erdira heldu arte.
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
	}

	if ((pertsonaiaX + mapaX >= 775) && (bizitzaBossTotal > 0))
	{
		helduDa = 1;
	}

	/*
	if (bizitzaBossTotal == 3)
	{
		if (pertsonaiaX <= 600)
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else if (mapaX < 175 && pertsonaiaX > 600)
		{
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			helduDa = 1;
		}
	}
	else if (bizitzaBossTotal == 0)
	{
		if (pertsonaiaX <= 600 || (mapaX >= 300 && pertsonaiaX < 1100))
		{
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			pertsonaiaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}
		else if (mapaX < 300)
		{
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
			SDL_Delay(50);
			mapaX += 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk2.bmp");
			SDL_Delay(50);
		}

	}*/
}

void EzkerreraMB()
{
	kolisioaDetektatuMB(pertsonaiaX - 30, pertsonaiaY);
	
	if (kolisioa == 0)
	{
		if ((mapaX + pertsonaiaX - 30 >= 600) && (mapaX + pertsonaiaX - 30 < 900))
		{
			mapaX -= 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			mapaX -= 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
		else
		{
			pertsonaiaX -= 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
			SDL_Delay(50);
			pertsonaiaX -= 15;
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
			SDL_Delay(50);
		}
	}
	/*if (pertsonaiaX > 15 && mapaX <= 0)
	{
		pertsonaiaX -= 15;
		pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		SDL_Delay(50);
		pertsonaiaX -= 15;
		pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
		SDL_Delay(50);
	}
	else if (pertsonaiaX >= 600 && mapaX > 0)
	{
		mapaX -= 15;
		pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		SDL_Delay(50);
		mapaX -= 15;
		pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk2.bmp");
		SDL_Delay(50);
	}*/
}

void SaltoaMarraztuMB(float t0, int y0, int vy)
{
	float t = SDL_GetTicks64();
	float T = (t - t0) / 120;
	int Y;

	Y = y0 - vy * T + 0.5 * 9.8 * T * T;

	SDL_Event event;

	if (SDL_PollEvent(&event) != 0)
	{
		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_LEFT:
				EzkerreraMB();
				norabideaPertsonaiaMB = 2;
				break;
			case SDLK_RIGHT:
				EskumaraMB();
				norabideaPertsonaiaMB = 1;
				break;
			}
		}
	}

	kolisioaDetektatuMB(pertsonaiaX, Y);

	if (kolisioa == 0)
	{
		pertsonaiaY = Y;
		if (norabideaPertsonaiaMB == 1)
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}
		SaltoaMarraztuMB(t0, y0, vy);
	}
	else
	{
		JausiMB();
	}
}

void JausiMB()
{
	kolisioaDetektatuMB(pertsonaiaX, pertsonaiaY + 5);

	while (kolisioa == 0)
	{
		pertsonaiaY += 5;
		if (norabideaPertsonaiaMB == 1)
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEsk1.bmp");
		}
		else
		{
			pertsonaiaMarraztuMB(".\\Argazkiak\\Pertsonaiak-bmp\\PertsonaiaEzk1.bmp");
		}

		kolisioaDetektatuMB(pertsonaiaX, pertsonaiaY + 5);
	}
}

void ariketarenEmaitzaOndoTxarto()
{
	int lehenZenbakia, bigarrenZenbakia, eragiketa, jokalariarenErantzuna;
	char str[128];

	eragiketa = zenbakiaEragiketaSortu(3); //4

	if (eragiketa == 0 || eragiketa == 1)
	{
		lehenZenbakia = zenbakiaEragiketaSortu(100);
		bigarrenZenbakia = zenbakiaEragiketaSortu(100);
		if (eragiketa == 0)
		{
			printf("\n%d + %d = ", lehenZenbakia, bigarrenZenbakia);
			fgets(str, 128, stdin);
			sscanf(str, "%d", &jokalariarenErantzuna);

			if (lehenZenbakia + bigarrenZenbakia == jokalariarenErantzuna) //emaitza ondo dagoen edo ez
			{
				emaitzaOndoTxarto = 1;
			}
			else
			{
				emaitzaOndoTxarto = 0;
			}
		}
		else
		{
			printf("\n%d - %d = ", lehenZenbakia, bigarrenZenbakia);
			fgets(str, 128, stdin);
			sscanf(str, "%d", &jokalariarenErantzuna);

			if (lehenZenbakia - bigarrenZenbakia == jokalariarenErantzuna)
			{
				emaitzaOndoTxarto = 1;
			}
			else
			{
				emaitzaOndoTxarto = 0;
			}
		}
	}
	else if (eragiketa == 2)
	{
		lehenZenbakia = zenbakiaEragiketaSortu(20);
		bigarrenZenbakia = zenbakiaEragiketaSortu(20);

		printf("\n%d x %d = ", lehenZenbakia, bigarrenZenbakia);
		fgets(str, 128, stdin);
		sscanf(str, "%d", &jokalariarenErantzuna);

		if (lehenZenbakia * bigarrenZenbakia == jokalariarenErantzuna)
		{
			emaitzaOndoTxarto = 1;
		}
		else
		{
			emaitzaOndoTxarto = 0;
		}
	}
	/*
	else if (eragiketa == 3)
	{
		lehenZenbakia = zenbakiaSortu(100);
		bigarrenZenbakia = zenbakiaSortu(100);
		while (lehenZenbakia % bigarrenZenbakia != 0 && bigarrenZenbakia > lehenZenbakia)
		{
			lehenZenbakia = zenbakiaSortu(100);
			bigarrenZenbakia = zenbakiaSortu(100);
		}
		printf("%d / %d = ", lehenZenbakia, bigarrenZenbakia);
		fgets(str, 128, stdin);
		sscanf(str, "%", &jokalariarenErantzuna);
	}
	*/
}

int zenbakiaEragiketaSortu(int nonaino)
{
	int zenbakia;

	zenbakia = rand() % nonaino; //sortzen diren eragiketa guztiak hausazkoak dira. Bai zenbakiak eta +, - edo x den ere bai

	return zenbakia;
}

void bossMugimenduaMB()
{
	if (emaitzaOndoTxarto == 0) //jokalariaren emaitza ez baldin badago ondo, etsaia mugitu egingo da.
	{
		bossX -= 60;
		bossaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Etsaiak\\German.bmp");
	}
}

void kolisioaDetektatuMB(int x, int y)
{
	kolisioa = 0;

	y += 125;
	x += mapaX + 35;

	if ((x <= 0) || (x >= 1500))
	{
		kolisioa = 1;
	}
	else if (y > 350)
	{
		kolisioa = 1;
	}
}

void kolisioaPertsonaiaBossMB()
{
	kolisioaPertsonaiaBoss = 0;

	if (pertsonaiaX > bossX - 200)
	{
		kolisioaPertsonaiaBoss = 1;
	}
	else if (bizitzaBossTotal == 0)
	{
		if (mapaX > 250 && pertsonaiaX > 900 && pertsonaiaY > 174)
		{
			irten = 2;
		}
	}
}

void hilMB(int nor)
{
	while ((pertsonaiaY < 600) && (bossY < 600))
	{
		mapaMarraztuMB();

		SDL_Rect dstrect;

		dstrect.x = pertsonaiaX;
		dstrect.y = pertsonaiaY;
		dstrect.w = 69;
		dstrect.h = 125;

		SDL_RenderCopy(Render, pertsonaiaT, NULL, &dstrect);

		bizitzaMarraztu(bizitzaM);

		if (nor == 1) //pertsonaia hil egingo da
		{
			bossaMarraztuMB(".\\Argazkiak\\Mate-bmp\\Etsaiak\\German.bmp");
			pertsonaiaY += 7;
		}
		else //etsaia hil egingo da
		{
			bossY += 7;

			SDL_Rect dstrect;

			dstrect.x = bossX;
			dstrect.y = bossY;
			dstrect.w = 103;
			dstrect.h = 162;

			//SDL_BlitSurface(Boss, NULL, Superficie, &dstrect);
			//SDL_UpdateWindowSurface(screen);

			SDL_RenderCopy(Render, BossT, NULL, &dstrect);
		}
		SDL_RenderPresent(Render);
	}
}